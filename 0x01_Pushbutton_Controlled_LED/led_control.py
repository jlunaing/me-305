'''!
@file       led_control.py
@brief      This program makes three LED patterns with push button control.
@details    Program alternates between 3 LED patterns: a square wave, 
            a sine wave, and a sawtooth wave. The patterns are displayed
            on the A5 pin of the STM32 Nucleo board. The brightness of the 
            LED is accomplished through PWM.
            
            More details in 
            <a href = "https://bitbucket.org/jlunaing/me-305/src/master/0x01_Pushbutton_Controlled_LED/">
            source code</a>,
            @ref writeup_0x01_led "documentation", and
            <a href = "https://youtu.be/APnAdVtXYuA">demo video</a>.

            State-transition diagram for FSM:

            @image  html    0x01_fsm.png "" width = 85%

@author     Juan Luna
@date       2021-10-04 Original file
@date       2022-12-22 Modified for portfolio update

'''

import math
import pyb
import utime

def onButtonPress(IRQ_src):
    '''! @brief  This function changes the LED pattern when button is pressed.
         @param  IRQ_src Variable from interrupt callback function
    '''
    print("\nThe button has been pressed!")
    
    global buttonFlag
    buttonFlag = True

def squareWave(dt):
    '''! @brief  Function that controls LED brightness in a square wave pattern.
         @param  dt  Time difference between instances at which time was recorded.
    '''
    t2ch1.pulse_width_percent(100*((dt/1000) % 1.0 < 0.5))  
    
def sineWave(dt):
    '''! @brief  Function that controls LED brightness in a sine wave pattern.
         @param  dt  Time difference between instances at which time was recorded.
    '''
    t2ch1.pulse_width_percent(50*(1 + math.sin(0.2*math.pi*(dt/1000))))

def sawtoothWave(dt):
    '''! @brief  Function that controls LED brightness in a sawtooth wave pattern.
         @param  dt  Time difference between instances at which time was recorded.
    '''
    t2ch1.pulse_width_percent(100*((dt/1000) % 1.0))
    
def turnOffLED():
    '''! @brief  Function that sets the LED brightness to 0.
    '''    
    t2ch1.pulse_width_percent(0) 

def resetTimer():
    '''! @brief     This function resets timer that starts by pressing button.
         @details   Record "start" timestamp in ms.
         @param  startTime   First timestap.
    '''  
    global startTime
    startTime = utime.ticks_ms()    # Start timer
    
def updateTimer():
    '''! @brief  This function updates the timer started before by button pressed.
         @details    Record "stop" timestamp in ms and measure time elapsed.
         @param  stopTime    Second timestap.
         @param  dt  Time difference between instances at which time was recorded.
    '''  
    global dt
    stopTime = utime.ticks_ms()     # Stop timer
    dt = utime.ticks_diff(stopTime, startTime)    

# Main program runs continuously until the user exits.
if __name__ == '__main__':
       
    ## Pin object for PC13, representing the button to be pressed.
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13) 
    ## Pin object for blinking LED on analog pin A5.
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    ## Timer object using timer 2 at 20-kHz frequency.
    tim2 = pyb.Timer(2, freq = 20000)
    ## Channel object in PWM mode and set pin A5 as timer channel.
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin = pinA5)
    ## External interrupt caused by button pressed. 
    ButtonInt = pyb.ExtInt(pinC13, mode = pyb.ExtInt.IRQ_FALLING,
                                   pull = pyb.Pin.PULL_NONE,
                               callback = onButtonPress)
    ## The current state for this iteration of the FSM
    state = 0
    ## Number of iterations performed by FSM
    run = 0
    
    resetTimer()
    ## Boolean variable representing "pressed" state of LED
    buttonFlag = False
    
    while True:
        # Attempt to run FSM unless Ctrl+C is hit
        try:
            # Initialization and welcome state.
            if (state == 0):
                # "Discard" LED pattern from any previous state.
                turnOffLED()
                # Welcome user and give basic instructions.
                print("Hello, user. Thanks for running this program today.\n"
                      "To cycle through three LED patterns, press the blue "
                      "button on the Nucleo board.")
                # Transition to state 1
                state = 1   
            
            # This state waits for button to be pressed.
            elif state == 1:
                # If button is pressed, iteratively transition from
                # state 2 to 4 sequentially and then repeat.
                if buttonFlag == True:
                    resetTimer()    # Start timer
                    state = 2
                    print("Square wave LED pattern selected.")
                
                buttonFlag = False
                updateTimer()   # Stop timer and record time difference
            
            # Square wave pattern state
            elif state == 2:
                if buttonFlag == True:
                    resetTimer()    # Start timer
                    state = 3
                    print("Sine wave LED pattern selected.")
                
                buttonFlag = False
                updateTimer()   # Stop timer and record time difference
                squareWave(dt)
                
            # Sine wave pattern state
            elif (state == 3):
                if (buttonFlag == True):
                    resetTimer()    # Start timer
                    state = 4
                    print("Sawtooth wave LED pattern selected.")
                
                buttonFlag = False
                updateTimer()   # Stop timer and record time difference
                sineWave(dt)
                
            # Sawtooth wave pattern state
            elif (state == 4):
                if (buttonFlag == True):
                    resetTimer()    # Start timer
                    state = 2
                    print("Square wave LED pattern selected.")
                
                buttonFlag = False
                updateTimer()   # Stop timer and record time difference
                sawtoothWave(dt)
           
            # Increment the run counter
            run += 1
            
        # Look for Ctrl+C which triggers a KeyboardInterrupt
        except KeyboardInterrupt:
            break
    
    turnOffLED()    # Turn off LED at the end of the program
    
    print("\nProgram terminating..."
          "\nThanks for testing my program.")