'''!   
@file       main_0x05.py
@brief      Main script for overall execution of Project 0x05.
@details    Calls @c BNO055.py to calibrate IMU and display orientation
            data in the form of Euler angles (heading, pitch, and roll).
            
            More details in 
            <a href = "https://bitbucket.org/jlunaing/me-305/src/master/0x05_I2C_with_IMU/">
            source code</a> and @ref writeup_0x05_imu "documentation".

@author     Juan Luna
@date       2021-11-08  Original file
@date       2022-12-22  Modified for portfolio update
'''

from pyb import I2C
import BNO055
import utime

if __name__ == '__main__':
    # The following code is a test program for the motor class.
    # All code within the if __name__ == '__main__' block will only run 
    # when the script is executed as a standalone program. If the script 
    # is imported as a module the code block will not run.

    ## Serial clock rate, in Hz
    BAUDRATE = 400_000

    ## Create I2C object attached to bus 1.
    #  Bus is initialized in controller/master mode
    i2c = I2C(1, I2C.MASTER)
    
    ## Initialize I2C bus with following parameters:
    #  MODE: master/controller
    #  BAUDRATE: SCL clock rate
    i2c.init(I2C.MASTER, BAUDRATE)
    
    ## Create IMU driver object with following parameters:
    # ADDR: I2C address is 0x28
    # I2C: I2C object
    imu_drv = BNO055.BNO055(0x28, i2c)
    
    ## Logical flag for calibration, becomes true when calibration
    #  is completed.
    is_calib = False
    
    while (is_calib != True):
        ## Attempt to run unless Ctrl+C is hit
        print("Calibrating...")
        print("(Sys,\tGyr,\tAcc,\tMag)")
        try:
            ## Get calibration status of the IMU
            status = imu_drv.get_calib_stat()
            print("Status: " + str(status))
            utime.sleep(0.5)
            
            ## Calibration is finished
            if (status[0]*status[1]*status[2]*status[3] == 3**4):
                is_calib = True
                print("The IMU has been calibrated!")
                print("Calibration Coefficient:\n" + 
                      str(imu_drv.get_calib_coeff()) + "\n")
                while (True):
                    ## Attempt to run FSM unless Ctrl+C is hit
                    try:
                        ## Print Euler angles 
                        print("Heading, roll, pitch:\n" 
                        + str(imu_drv.read_euler_angles()) + "\n")
                        ## Slow down iterations
                        utime.sleep(1.5)
                        ## Break out of loop on Ctrl+C
                    except KeyboardInterrupt:                        
                        break
        ## Break out of loop on Ctrl+C    
        except KeyboardInterrupt:                        
            break
    ## Turn off I2C bus on exit
    imu_drv.turn_off()
    print("Program Terminating...")