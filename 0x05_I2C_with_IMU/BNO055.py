'''!    @file       BNO055.py
        @brief      A driver class for interfacing with the BNO055 sensor.
        @author     Juan Luna
        @date       2021-11-08 Original file
        @date       2022-12-22 Modified for portfolio update
'''

import pyb
import struct

## Calibration status
CALIB_STAT      = 0x35
## Calibration coefficient, up to 0x6A
CALIB_START     = 0x55
## Euler angle data registries, up to 0x1F
EULER_START     = 0x1A
## Gyroscope data registries, up to 0x19
GYR_START       = 0x14
## NDOF mode offers all three sensor outputs and fuses the data using
#  absolute orientation (See Table 3-3, 3-5 in datasheet)
NDOF_MODE       = 0x0C

# Buffer length

## Buffer length for status
status_size     = 1
## Buffer length for coefficients
coeff_size      = 22
## Buffer length for euler angles
angles_size     = 6
## Buffer length for velocity
vel_size        = 6


class BNO055:
    '''! @brief      A driver class for the BNO055 IMU sensor.
         @details    Encapsulates funcionality of BNO055 sensor. This driver
                     instantiates objects and defines methods that collectively
                     allow program to obtain orientation information in the
                     form of Euler angles (heading, pitch, and roll) and 
                     angular velocity data from gyroscopic data.

                     A calibration method is responsible for calibrating the
                     resistive touch panel to improve accuracy by accounting
                     for discrepancies between ideal and actual physical panels. 
    '''
    def __init__(self, address, i2c):
        '''! @brief      Initializes future objects of the BNO055 class.
        '''
        ## I2C device address
        self.address = address
        ## I2C device object
        self.i2c = i2c
        ## 9-DOF operating mode
        self.set_mode(NDOF_MODE)

        # Buffer bytearrays

        ## Buffer length for status
        self.buf_status = bytearray(status_size)
        ## Buffer length for coefficients
        self.buf_coeff  = bytearray(coeff_size)
        ## Buffer length for euler angles
        self.buf_angles = bytearray(angles_size)
        ## Buffer length for velocity
        self.buf_vels   = bytearray(vel_size)

    def turn_off(self):
        '''! @brief      Turn off I2C bus when exiting program
        '''
        self.i2c.deinit()

    def set_mode(self, data):
        '''! @brief      Changes operating mode of the IMU,
             @details    Based on user input, writes operating mode to IMU
                         register address.
        '''
        ## OPERATING_MODE = 0x3D
        self.i2c.mem_write(data, self.address, 0x3D)
    
    def get_calib_stat(self):
        '''! @brief      Gets calibration status of the IMU
        '''
        ## CALIB_STAT = 0x35
        self.i2c.mem_read(self.buf_status, self.address, CALIB_STAT)
        
        cal_status = (self.buf_status[0] & 0b11,
                     (self.buf_status[0] & 0b11 << 2) >> 2,
                     (self.buf_status[0] & 0b11 << 4) >> 4,
                     (self.buf_status[0] & 0b11 << 6) >> 6)

        return cal_status
        
    def get_calib_coeff(self):
        '''! @brief      Gets calibration coefficients to the IMU as binary data
        '''
        ## CALIB_START: 0x55 to 0x6A
        self.i2c.mem_read(self.buf_coeff, self.address, CALIB_START)
        ## Unpack binary data
        return tuple(struct.unpack('<hhhhhhhhhhh', self.buf_coeff))
        
    def write_calib_coeff(self, data):
        '''! @brief      Writes calibration coefficients from the IMU.
        '''
        ## CALIB_START: 0x55 to 0x6A
        self.i2c.mem_write(data, self.address, CALIB_START)
    
    def read_euler_angles(self):
        '''! @brief      Reads Euler angles from the IMU.
             @details    Reads serial data (over I2C) from IMU and returns 
                         heading, pitch, and roll values as a tuple.
             @return     Euler angles as tuple data type.   
        '''
        ## EULER_START: 0x1A to 0x1F 
        self.i2c.mem_read(self.buf_angles, self.address, EULER_START)
        ## First, unpack binary data into signed integers
        signed_euler = struct.unpack('<hhh', self.buf_angles)
        ## Second, scale values to get proper units
        euler_values = tuple(eul_int/16 for eul_int in signed_euler)
        
        return euler_values        
        
    def read_velocity(self):
        '''! @brief      Reads angular velocity from the IMU.
             @return     Velocity values as tuple data type.
        '''
        ## GYR_START: 0x14 to 0x19
        self.i2c.mem_read(self.buf_vels, self.address, GYR_START)
        ## Unpack binary data
        signed_vel = struct.unpack('<hhh', self.buf_vels)
        ## Scale values to get proper units
        vel_values = tuple(omg_int/16 for omg_int in signed_vel)

        return vel_values