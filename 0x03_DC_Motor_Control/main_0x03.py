'''!
@file       main_0x03.py
@brief      Main script for overall execution of Project 0x03: Motor Control
@details    Imports @c task_encoder_0x03.py, @c task_user_0x03.py, and 
            @c task_motor_0x03.py modules to control two DC motors with PWM. 
            Information about the position and delta (velocity) of the motors 
            is handled by two encoders. A simple user interface displays 
            relevant motor information and gives user ability to set the duty 
            cycle of the motors, zero the position of the encoders, print out 
            data, and clear fault conditions.

            More details in 
            <a href = "https://bitbucket.org/jlunaing/me-305/src/master/0x03_DC_Motor_Control/">
            source code</a> and @ref writeup_0x03_motor "documentation".
          
@author     Juan Luna
@date       2021-11-02  Original file
@date       2022-12-22  Modified for portfolio update     
'''

import DRV8847
import task_encoder_0x03
import task_motor_0x03
import task_user_0x03
import shares_0x03

def main():
    '''! @brief      The main program for Project 0x03.
    '''

    ## @brief       Task periods
    #  @details     Dict data type holding different task periods (in units
    #               of milliseconds).
    period          = {"enc_task": 10,
                       "motor_task": 10,
                       "user_task": 25}

    ## List containing relevant data from Motor 1 for sharing.
    M1_data = [1, 0, 0, 0, False, False]
    ## List containing relevant data from Motor 2 for sharing.
    M2_data = [2, 0, 0, 0, False, False]
    
    # Items in the lists:
    # Encoder/Motor ID (either 1 or 2), current position, current
    # delta/velocity, duty cycle, logical value for zeroing condition
    # (True/False), and logical value for fault condition (True/False).
    # For extensive details, see "task_motor.py" or "task_user.py"

    ## Motor driver object of the DRV8847 class
    motor_drv = DRV8847.DRV8847(3)

    ## Data sharing object containing shared data for the motors.  
    M1_shares = shares_0x03.Share(M1_data)
    M2_shares = shares_0x03.Share(M2_data)

    ## User task object for the user interface.
    user_task = task_user_0x03.Task_User(period["user_task"], M1_shares, M2_shares)

    ## Encoder task object for the first encoder.   
    E1_task = task_encoder_0x03.Task_Encoder(period["enc_task"], M1_shares)
    ## Encoder task object for second encoder.  
    E2_task = task_encoder_0x03.Task_Encoder(period["enc_task"], M2_shares)
    
    ## Motor task object for the first motor.
    M1_task = task_motor_0x03.Task_Motor(period["motor_task"], M1_shares, motor_drv)
    ## Motor task object for the second motor.
    M2_task = task_motor_0x03.Task_Motor(period["motor_task"], M2_shares, motor_drv)
    
    ## List of tasks to run
    task_list = [E1_task, E2_task, user_task, M1_task, M2_task]

    while (True):
        ## Attempt to run FSM unless Ctrl+C is hit
        try:
            for task in task_list:
                task.run()

        ## Break out of loop on Ctrl+C
        except KeyboardInterrupt:
            break
        
    ## Disable motor driver and terminate
    motor_drv.disable()
    print("Program terminating...\n"
          "Thank you for using this program.")

if __name__ =='__main__':
    main()