'''!
@file       task_encoder_0x03.py
@brief      This module implements an encoder task for Lab 0x03.
@details    Implements functionality of encoder driver created in 
            @c encoder_0x03.py into an encoder task that interacts with user 
            interface and motor tasks.

@author     Juan Luna
@date       2021-11-02  Original file
@date       2022-12-22  Modified for portfolio update
'''

import encoder_0x03
import math
import utime
import pyb

## @brief       Dictionary with descriptive names for items in shared data
#  @details     Stores descriptive names for items in the list that contains
#               shared motor data. This list is defined in main and has the 
#               following items: encoder ID [index 0], logical value for
#               zeroing condition [index 1] (True: zero encoder requested
#               or False: zero encoder not requested), logical value for
#               fault condition [index 2] (True: disable fault or False:
#               do not disable fault), current position [index 3], current
#               delta/velocity [index 4], and duty cycle [index 5].
motor_info      = {"id": 0, "position": 1, "delta": 2, "duty_cycle": 3,
                   "zeroing": 4, "fix_fault": 5}

## Conversion between encoder "ticks" and radians
tick2rad        = 2*(math.pi)/4000

## @brief       First pin object associated with Encoder 1
pin1_ENC1       = pyb.Pin(pyb.Pin.cpu.B6)
## @brief       Second pin object associated with Encoder 1
pin2_ENC1       = pyb.Pin(pyb.Pin.cpu.B7)

## @brief       First pin object associated with Encoder 2
pin1_ENC2       = pyb.Pin(pyb.Pin.cpu.C6)
## @brief       Second pin object associated with Encoder 2
pin2_ENC2       = pyb.Pin(pyb.Pin.cpu.C7)

class Task_Encoder:
    '''! @brief         Encoder task implementing encoder driver functionality.
         @details       Tracks encoder position and delta. Communicates with 
                        @c task_user_0x03.py module to set encoder position to 
                        zero when user interface determines it is appropriate. 
    '''
    
    def __init__(self, period, motor_shares):
        '''! @brief              Constructs future encoder task objects
             @details            Instantiates attributes related to timing tasks,
                                 data sharing, and encoder objects.
             @param  period          Task period, specified in main script.
             @param  motor_shares    Array containing motor shared data.
        '''
        
        ## Period, in microseconds, defined in main program
        self.period = period
        ## Current time or first time stamp
        self.current_time = utime.ticks_ms()
        ## Second time stamp when timer reaches the next period.
        self.next_time = utime.ticks_ms() + period

        ## Relevant shared data for the motors
        self.shared_data = motor_shares
        
        ## Create two encoder objects
        if self.shared_data.read(motor_info["id"]) == 1:
            self.encoder = encoder_0x03.Encoder(4, pin1_ENC1, pin2_ENC1)

        elif self.shared_data.read(motor_info["id"]) == 2:
            self.encoder = encoder_0x03.Encoder(8, pin1_ENC2, pin2_ENC2)
    
    def run(self):
        '''!
        @brief          Resets encoder position and updates position and delta.
        @details        Access user commands from shared data to zero the encoder
                        (from Z command) and update position and delta values using 
                        the relevant update function (next_update).
        '''

        ## Update timer based on current time stamp to update position/delta values
        if (utime.ticks_ms() >= self.next_time):
            self.next_update()
                        
            ## Reset encoder if user decides to zero the position of encoder
            if self.shared_data.read(motor_info["zeroing"]):
                ## Set encoder position to zero
                self.encoder.set_position(0)
                ## Zeroing request flag is disabled/reset
                self.shared_data.write(motor_info["zeroing"], False)
        
    def next_update(self):
        '''!
        @brief      Updates encoder position and updates timer period.          
        @details    Encoder object is updated based on functionality in
                    encoder driver             
        '''
        self.encoder.update() 
        self.next_time += self.period

        ## Update values of the shared motor variables (position and delta)
        #  Position is registered in units of radians
        self.shared_data.write(motor_info["position"],
                                self.encoder.get_position()*tick2rad)
        #  "Delta" is registered as velocity in units of radians per second
        #  Since units of microseconds are used, we divide by 10^3 seconds
        self.shared_data.write(motor_info["delta"],
                                self.encoder.get_delta()*tick2rad/(self.period/1E3))