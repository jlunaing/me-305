'''!
@file       task_motor_0x03.py
@brief      This module implements a motor task for Project 0x03.
@details    Implements functionality of motor driver from @c DRV8847.py 
            into a motor task that interacts with user-interface and 
            encoder tasks, @c task_user_0x03.py and @c task_encoder_0x03.py
            respectively, in the main file @c main_0x03.py, where these
            tasks are imported as modules.

@author     Juan Luna
@date       2021-11-02  Original file
@date       2022-12-22  Modified for portfolio update
'''

import pyb
import utime

## @brief       Dictionary with descriptive names for items in shared data
#  @details     Stores descriptive names for items in the list that contains
#               shared motor data. This list is defined in main and has the 
#               following items: encoder ID [index 0], current position
#               [index 1], current delta/velocity [index 2], duty cycle
#               [index 5], logical value for zeroing condition [index 1]
#               (True: zero encoder requested  or False: zero encoder not
#               requested), and logical value for fault condition [index 2]
#               (True: disable fault or False: do not disable fault).
motor_info      = {"id": 0, "position": 1, "delta": 2, "duty_cycle": 3,
                   "zeroing": 4, "fix_fault": 5}

# Motor pins

## @brief       First MCU control pin associated with first motor.
#  @details     First motor is connected to pins OUT1 and OUT2,
#               which are commanded by the MCU pins IN1 AND IN2.
pin_IN1          = pyb.Pin(pyb.Pin.cpu.B4)
## @brief       Second MCU control pin associated with first motor.
#  @details     First motor is connected to pins OUT1 and OUT2,
#               which are commanded by the MCU pins IN1 AND IN2.
pin_IN2          = pyb.Pin(pyb.Pin.cpu.B5)
## @brief       First MCU control pin associated with second motor.
#  @details     Second motor is connected to pins OUT3 and OUT4,
#               which are commanded by the MCU pins IN3 and IN4.
pin_IN3          = pyb.Pin(pyb.Pin.cpu.B0)
## @brief       Second MCU control pin associated with second motor.
#  @details     Second motor is connected to pins OUT3 and OUT4,
#               which are commanded by the MCU pins IN3 and IN4.
pin_IN4          = pyb.Pin(pyb.Pin.cpu.B1)

class Task_Motor:
    '''! @brief      Motor task implementing motor driver functionality.
         @details    Keeps track of encoder position and delta, while also
                     setting calling the set position function in driver to
                     when zero boolean is true.
    '''
    
    def __init__(self, period, motor_shares, motor_driver):
        '''!    @brief      Constructs future motor task objects
                @details    Instantiates objects responsible for timing tasks,
                            data sharing, and motor objects.

            @param  period          Period of task, specified in main script.
            @param  motor_shares    Array containing motor shared data.
            @param  motor_driver    Motor driver object of the @c DRV8847 class.
        '''

        ## Period, in microseconds, defined in main program
        self.period = period
        ## Current time or first time stamp
        self.current_time = utime.ticks_ms()
        ## Second time stamp when timer reaches the next period
        self.next_time = utime.ticks_ms() + period

        ## Relevant shared data for the motors
        self.shared_data = motor_shares
        ## Create a motor driver object of the DRV8847 class
        self.DRV8847_obj = motor_driver

        # Two motor objects of the DRV8847 motor driver class
        #  These are separate motor objects that cab be used independently
        #  but having the parent's object configuration.
        if (self.shared_data.read(motor_info["id"]) == 1):
            ## First motor object
            self.motor = self.DRV8847_obj.motor(1, pin_IN1, pin_IN2)
        
        elif (self.shared_data.read(motor_info["id"]) == 2):
            ## Second motor object
            self.motor = self.DRV8847_obj.motor(3, pin_IN3, pin_IN4)

        ## Bring DRV8847 out of sleep mode
        self.DRV8847_obj.enable()
        ## Set duty cycle as a pulse width percent
        self.motor.set_duty(self.shared_data.read(motor_info["duty_cycle"]))
    
    def run(self):
        '''!
        @brief      Disables fault condition and sets duty cycle
        @details    Access user commands from shared data to clear fault 
                    condition and set the duty cycle. If fault condition is 
                    disabled, duty cycle is reset to zero. 
        '''

        # Update timer based on current time stamp
        if (utime.ticks_ms() >= self.next_time):
            self.next_time += self.period
            
            ## Enable motor if the user decides to clear fault
            if (self.shared_data.read(motor_info["fix_fault"])):

                self.DRV8847_obj.enable()
                print("Motor enabled!")
                ## Update values of the shared motor variables
                #  Duty cycle is reset to zero
                self.shared_data.write(motor_info["duty_cycle"], 0)
                #  Fault condition condition flag is set to not disabled
                self.shared_data.write(motor_info["fix_fault"], False)

            ## Set duty cycle by access the registered value of duty cycle    
            self.motor.set_duty(self.shared_data.read(motor_info["duty_cycle"]))