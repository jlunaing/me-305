'''!
@file       task_user_0x03.py
@brief      This module controls and implements user interface for Project 0x03.
@details    Implements a simple data collection interface and facilitates
            interaction with encoder and motor task objects.

            This task handles all serial communication between user and
            backend running on Nucleo. It accepts user input from a lists of 
            available commands.
    
@author     Juan Luna
@date       2021-11-02  Original file
@date       2022-12-22  Modified for portfolio update
'''

import array
import pyb
import utime

## State 0 variable for "initializion" state.
S0_INIT = 0
## State 1 variable for "waiting for input" state.
S1_WAIT_FOR_INPUT = 1

## @brief       Dictionary with descriptive names for items in shared data
#  @details     Stores descriptive names for items in the list that contains
#               shared motor data. This list is defined in main and has the 
#               following items: encoder ID [index 0], current position
#               [index 1], current delta/velocity [index 2], duty cycle
#               [index 5], logical value for zeroing condition [index 1]
#               (True: zero encoder requested  or False: zero encoder not
#               requested), and logical value for fault condition [index 2]
#               (True: disable fault or False: do not disable fault).
motor_info      = {"id": 0, "position": 1, "delta": 2, "duty_cycle": 3,
                   "zeroing": 4, "fix_fault": 5}

## @brief       VCP object to handle serial communication over USB.
#  @details     Receive characters user types in command prompt, as they 
#               are received, using this VCP. In this way, it facilitates
#               communication between user running PuTTY and Nucleo board.
serialComm      = pyb.USB_VCP()

# Define a class for user interface
class Task_User:
    '''! @brief          User task class for user interface.
         @details        Facilitates interaction between user running Putty and
                         Nucleo board using a user interface. This interface
                         gives a list of available commands and prompts user to
                         enter their desired command. 
    '''
    
    def __init__(self, period, M1_shares, M2_shares):
        '''! @brief          Construct future user task objects
             @details        Instantiates attributes related to communication
                             over virtual serial port, timing as it relates to
                             user interface and data (i.e., position or delta
                             values) print statements, and similar.
             @param  period      Period of task, specified in main script.
             @param  M1_shares   Array containing motor shared data for motor 1.
             @param  M2_shares   Array containing motor shared data for motor 2.
        '''
     
        ## The state to run on the next iteration of the task.
        self.state = S0_INIT
        ## Counter tracking the number of times the task has run.
        self.runs = 0
        ## Period is set as the value specified in main script.
        self.period = period
        ## Current time
        self.current_time = utime.ticks_ms()
        ## Time is updated once clock reaches period plus the current time
        self.next_time = self.current_time + period

        ## Share variable for first motor
        self.M1_shares = M1_shares
        ## Share variable for second motor
        self.M2_shares = M2_shares

        ## @brief       Encoder time data
        #  @details     Array for collecting encoder time data associated with
        #               "G"/"g" user key command. The array.array Python type
        #               is used for memory efficiency.
        self.timeArray  = [array.array('f', []), array.array('f', [])]
        ## @brief       Encoder position data
        #  @details     Array for collecting encoder position data associated
        #               with "G"/"g" user key command. The array.array Python
        #               type is used for memory efficiency.
        self.PosArray   = [array.array('f', []), array.array('f', [])]
        ## @brief       Encoder delta (velocity) data
        #  @details     Array for collecting encoder velocity data associated 
        #               with "G"/"g" user key command. The array.array Python
        #               type is used for memory efficiency.
        self.VelArray   = [array.array('f', []), array.array('f', [])]

        ## @brief           Data collection logical value for motors 1 and 2.
        #  @details         Initialized as false, the data collection flag
        #                   becomes true when user request data collection
        #                   through the G command.
        self.collect_data   = [False, False]

        ## @brief           Set duty cycle logical value for motors 1 and 2.
        #  @details         Initialized as false, the construct duty cycle
        #                   flag is raised when user enters duty cycle
        #                   through the M command.
        self.construct_duty = [False, False]

        ## @brief           List with characters to construct duty cycle.
        #  @details         Characters entered by the user when entering the
        #                   desired duty cycle will be pre-processed so that
        #                   a valid value for the duty cycle is entered.
        self.parsing        = [0, 0]

    def run(self): 
        '''! @brief      Run relevant functions required by user task.
             @details    Transitions through states of the FSM and displays
                         a user interface at initialization state. Reads and
                         stores user input through serial for later usage.
        '''
        
        ## Update timer based on current time stamp
        if (utime.ticks_ms() >= self.next_time): 
            self.next_time += self.period
            
            ## State 0 code
            if (self.state == S0_INIT):
                ## Display list of commands
                print("\033c", end = "")
                print("----------------------------------------------------\n"
                      "USER COMMANDS:\n"
                      "\tz:\tZero the position of encoder 1\n"
                      "\tZ:\tZero the position of encoder 2\n"
                      "\tp:\tPrint out the position of encoder 1\n"
                      "\tP:\tPrint out the position of encoder 2\n"
                      "\td:\tPrint out the delta for encoder 1\n"
                      "\tD:\tPrint out the delta for encoder 2\n"
                      "\tm:\tEnter a duty cycle for motor 1\n"
                      "\tM:\tEnter a duty cycle for motor 2\n"
                      "\tc:\tClear fault condition triggered by the DRV8847\n"
                      "\tg:\tCollect encoder 1 data for 30 s and\n"
                      "\t\tprint it to PuTTY as a comma separated list\n"
                      "\tG:\tCollect encoder 2 data for 30 s and\n"
                      "\t\tprint it to PuTTY as a comma separated list\n"
                      "\ts:\tEnd data collection prematurely.\n"
                      "\tq:\tShow list of command again.\n\n"
                      "Enter a command...\n")
                
                ## Transision to state 1
                self.transition_to(S1_WAIT_FOR_INPUT) 
            
            ## State 1 code
            elif (self.state == S1_WAIT_FOR_INPUT):
                ## Read user input
                user_command = self.read()
                ## Store key command returned by user       
                self.write(user_command[0], self.M1_shares)
                self.write(user_command[0] + 32, self.M2_shares)
        
        self.runs += 1

    def write(self, user_command, motor_shares):
        '''! @brief          Display information based on command selected by user.
             @details        Logic and decision making for receiving user command.
                             It prints a variety of information regarding encoders
                             and motors based on user input.

             @param  user_command   Key command received through serial port.
             @param  motor_shares   Share variable for motor data          
        '''
        
        ## Variables containing motor shared data at the index corresponding
        #  to the respective item from the list (see main.py, lines 34-37)
        id          = motor_shares.read(motor_info["id"]) - 1
        position    = motor_shares.read(motor_info["position"])
        velocity    = motor_shares.read(motor_info["delta"])
        duty        = motor_shares.read(motor_info["duty_cycle"])

        ## Logic/decision-making based on user input

        # Go back to State 0 the initial state when the "q" key is hit
        if (user_command == b'q'[0]):
            self.transition_to(S0_INIT)

        # If user selects "z", request for zeroing encoder is set to True.
        elif (user_command == b'z'[0]):
            motor_shares.write(motor_info["zeroing"], True)
    
        # If user selects "d", print encoder delta value.
        elif (user_command == b'd'[0]):
            print("Velocity [rad/s]: " + str(velocity))   

        # If user selects "p", print encoder position value.
        elif (user_command == b'p'[0]):
            print("Position [rad]: " + str(position))

        # If user selects "g", collect data for 30 sec.
        elif (user_command == b'g'[0] and not self.collect_data[id]):
            print("Collecting Data...\n")
            self.collect_data[id] = True
            # Set initial time for collecting data
            self.clock = utime.ticks_ms()

        # If user selects "m", prompt user to enter duty cycle
        elif (user_command == b'm'[0]):
            # Flag for setting duty cycle is turned on
            self.construct_duty[id] = True
            self.parsing[id] = ''    
        
        # If user selects "c", clear triggered fault condition
        elif (user_command == b'c'[0]):
            motor_shares.write(motor_info["fix_fault"], True)
            print("The fault has been fixed.\n")

        ## When data collection is requested by user (G command), record data 
        #  function is called with position and velocity values at ith time. 
        #  Terminating data prematurely (S command) is sent as a logical value.
        if (self.collect_data[id]):
            self.record_data(id, position, velocity, user_command == b's'[0])

        ## When user enters duty cycle (M command), the set duty function is
        #  called with the relevant variables as arguments.
        if (self.construct_duty[id]):
            processed_duty = self.set_duty(id, user_command, duty)
            if (processed_duty != duty):
                motor_shares.write(motor_info["duty_cycle"], processed_duty)

    def read(self):     
        ''' @brief      Receives characters entered in PuTTY while program runs.
            @details    Characters read are stored as bytes using a VCP object.
                        Clears queue and returns byte value.
        '''       
        if (serialComm.any()):
            ## Read Most recent command
            user_command = serialComm.read(1)
            ## Clear queue
            serialComm.read()

            return user_command

        return b' '

    def set_duty(self, num, user_char, duty):
        '''! @brief      Sets duty cycle entered by user.   
             @details    Process characters entered by user when setting duty
                         cycle so that duty cycle is a valid input to feed to 
                         the motors. This algorithm accepts one value at the
                         time and can take minus, backspace, and enter 
                         characters as ASCII bytes. 

             @param  num        Motor ID (motor 1 or 2), used as index.
             @param  user_char  Character entered in PuTTY by user.
             @param  duty       Duty cycle of the motor
             @return duty       Pre-processed duty cycle.
        '''
    
        user_char = user_char - (32*num)

        # Accept one value, between 0 and 9, at the time.
        if (user_char >= b'0'[0] and user_char <= b'9'[0]):
            new = str(user_char - 48)
            self.parsing[num] = self.parsing[num] + new

        elif (user_char == 127):
            self.parsing[num] = self.parsing[num][:-1]
        # If user presses the period key
        elif (user_char == 46):
            if "." not in self.parsing[num]:
                self.parsing[num] = self.parsing[num] + '.'
        # If user presses the minus key
        elif (user_char == 45):
            if self.parsing[num] == '' or self.parsing[num][0] != '-':
                self.parsing[num] = '-' + self.parsing[num]
        # If user presses carriage return
        elif (user_char == 13):
            # Flag for setting duty cycle is turned off
            self.construct_duty[num] = False

            # If no characters, set duty cycle to zero
            if (self.parsing[num] == ''):
                self.parsing[num] = '0'
            # Convert string to a float number
            return float(self.parsing[num])
        # If user presses the space bar
        if (user_char != 32):
            print("\033c", end = "")
            print("\nEnter duty cycle (0-100%): " + str(self.parsing[num]))
        return duty
        
    def record_data(self, ith, pos, vel, stop_req):
        '''! @brief          Records position and velocity time data.
             @details        Appends recorded data to time, position, and
                             velocity arrays. Data collection lasts 30 s
                             or less if premature stop condition (S command)
                             occurs. Data is printed at the end.

             @param ith         Motor ID (motor 1 or 2), used as index.
             @param pos         Position of the motor
             @param vel         Velocity of the motor
             @param stop_req    Logical value for premature data termination.
        '''   
       
        ## Current time in microseconds
        time_now = utime.ticks_ms()   
        
        # Formatting and printing output of "g" command.
        # Logic for "s" command in case "g" is interrupted is implemented.
        # In other words, unless manually stopped, data collection and 
        # display occurs for 30 s (30^3 ms).
        if  (stop_req or (utime.ticks_diff(utime.ticks_add(self.clock, 30_000), time_now) <= 0)):
            
            print("Time (s),\tPosition (rad),\t\tVelocity (rad/s)\n"
                  "---------------------------------------------------------")
            
            # Print interrupted or complete 30-s timed data
            for n in range(len(self.timeArray[ith])):
                print("{:.3f},\t\t{:.5f},\t\t{:.5f}".format(self.timeArray[ith][n]/(1_000),
                                                   self.PosArray[ith][n],
                                                   self.VelArray[ith][n]))

            # Flag for collecting data is turned off
            self.collect_data[ith] = False
            # Arrays empty when data recording and display ends
            self.timeArray[ith] = array.array('f', [])
            self.PosArray[ith]  = array.array('f', [])
            self.VelArray[ith]  = array.array('f', [])

        ## Append data to time, position, and velocity arrays
        else:
            self.timeArray[ith].append(utime.ticks_diff(time_now, self.clock))
            self.PosArray[ith].append(pos)
            self.VelArray[ith].append(vel)

    def transition_to(self, new_state):
        '''! @brief              Transitions between FSM states.
             @param  new_state   New state UI is transitioning to.
        '''
        self.state = new_state