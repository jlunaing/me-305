'''!
@file       main_0x06.py
@brief      Main script for overall execution of the Ball-Balancing Project.
@details    See extensive details in 
            <a href = "https://bitbucket.org/jlunaing/me-305/src/master/0x06_Balancing_Platform/">
            source code</a> and @ref writeup_0x06_project "documentation".

            State-transition diagram for FSM:

            @image  html    0x06_fsm.png "" width = 85%

            Feel free to check out my video that explains the system. There is 
            a demo a the very end.

            \htmlonly
            <center><iframe width="640" height="360" src="https://www.youtube.com/embed/_mb6aW0AfaI" 
                      title="YouTube video player" frameborder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                      allowfullscreen>
                    </iframe>
            </center>
            \endhtmlonly 
            
@author     Juan Luna
@date       2021-12-09  Original file
@date       2022-12-22  Modified for portfolio update
'''

import shares_0x06
import task_controller_0x06
import task_touch_panel_0x06
import task_IMU_0x06
import task_user_0x06

def main():
    '''! @brief     The main program for the Ball-Balancing Project.
         @details   Instantiates relevant objects and tasks for project.
    '''
    ## @brief       Task periods
    #  @details     Dict data type holding different task periods (in units
    #               of microseconds).
    period          = {"hardware_task": 5_000,
                       "IMU_task": 10_000,
                       "collection_task": 50_000,
                       "user_task": 20_000}

    ## Defining shares and queues variables for data sharing between tasks
    
    ## @brief       Data sharing variable for BNO055 IMU sensor.
    #  @details     Shares orientation data between tasks, including
    #               roll (theta_y), pitch (theta_x), change in roll
    #               (omega_y), and change in pitch (omega_x)
    IMU_shares      = shares_0x06.Share(())
    ## @brief       Queues variable for BNO055 IMU sensor.
    IMU_queue       = shares_0x06.Queue()

    ## @brief       Data sharing variable for touch panel sensor.
    #  @details     Shares horizontal x position, vertical y position,
    #               x velocity, y velocity, and "z" scan.
    RTP_shares      = shares_0x06.Share(())
    ## @brief       Queues variable for touch panel sensor.
    RTP_queue       = shares_0x06.Queue()

    ## @brief           Queues variable for motor duty cycle timing.
    motor_queue         = shares_0x06.Queue()

    ## @brief           Data sharing variable for balancing the ball
    balance_shares      = shares_0x06.Share(0)
    ## @brief           Data sharing variable for data collection
    collect_shares      = shares_0x06.Share(0)
    ## @brief           Data sharing variable for stopping data collection.
    collect_end_shares  = shares_0x06.Share(0)
    ## @brief           Data sharing variable for timing
    timer_shares        = shares_0x06.Share(0)
    
    ## Defining task objects

    ## @brief   Touch panel task object
    RTP_task    = task_touch_panel_0x06.RT_Task(period["hardware_task"], RTP_shares,
                                            balance_shares, collect_shares)
    ## @brief   BNO055 IMU sensor task object
    IMU_task    = task_IMU_0x06.Task_IMU(period["IMU_task"], IMU_shares,
                                    balance_shares, collect_shares)
    ## @brief   Controller system task object
    ctrl_task   = task_controller_0x06.Task_Control(period["hardware_task"],
                                                period["collection_task"],
                                                IMU_shares, IMU_queue,
                                                RTP_shares, RTP_queue,
                                                balance_shares, collect_shares,
                                                collect_end_shares, timer_shares,
                                                motor_queue)
    ## @brief   User interface task object
    user_task   = task_user_0x06.Task_User(period["user_task"],
                                        IMU_queue, RTP_queue, motor_queue,
                                        balance_shares, collect_shares,
                                        collect_end_shares, timer_shares)
    
    ## List of tasks to run
    task_list = [RTP_task, IMU_task, ctrl_task, user_task]

    while (True):
        ## Attempt to run FSM unless Ctrl+C is hit
        try:
            for task in task_list:
                task.run()

        ## Break out of loop on Ctrl+C
        except KeyboardInterrupt:
            break
        
    ## Terminate program message
    print("Program terminating...\n"
          "Thank you for using this program.")

if __name__ =='__main__':
    main()