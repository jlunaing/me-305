'''!    @file       motor_0x06.py
        @brief      A driver for working with a DC motor.
        @details    The motor driver encapsulates all of the functionality
                    that will be useful for interacting with the DC motor
                    as it pertains to the Term Project.

                    This driver will be implemented in a motor task
                    that will take the form of a controller task,
                    incorporating both motor functionality and closed-loop
                    control for balancing the ball.

        @author     Juan Luna
        @date       2021-12-07  Original file
        @date       2022-12-22  Modified for portfolio update
'''

import pyb

class Motor:
    '''! @brief      A class for the motor driver used in Project 0x06.
         @details    Objects of this class can be used to configure the
                     motor driver and create one or more objects of the
                     Motor class which can be used to perform motor control.
 
                     Objects of this class can be used to apply PWM to a
                     given DC motor.
    '''

    def __init__ (self, tim_ID, ch1, ch2, pin_ch1, pin_ch2):
        '''!
        @brief      Initializes motor object associated with the motor driver.
        @details    Attributes include timing and channel objects for 
                    operation of the DC motor. 
        '''
        ## Timer object for motor with 20-kHz frequency.
        self.timer_motor = pyb.Timer(tim_ID, freq = 20_000)

        ## Motor timer channel 1 configured in PWM mode (active high)
        self.tim2_ch1 = self.timer_motor.channel(ch1, mode = pyb.Timer.PWM, 
                                                      pin  = pin_ch1)
        ## Motor timer channel 2 configured in PWM mode (active high).
        self.tim2_ch2 = self.timer_motor.channel(ch2, mode = pyb.Timer.PWM, 
                                                      pin  = pin_ch2)
        
        ## Initialize motor objects with no duty cycle
        self.set_duty(0)

    def set_duty (self, duty):
        '''! @brief      Set duty cycle as a pulse width percent for the motors.
             @details    Positive values represent rotation of the motor in one
                         direction (clockwise) and negative values in the
                         opposite direction (counterclockwise).

             @param  duty    Signed value representing duty cycle of
                             PWM signal to be sent to the motor pins.            
        '''
        ## Changed "fast decay" driving to "slow decay" motor driving.
        #  See https://www.allaboutcircuits.com/technical-articles/difference-slow-decay-mode-fast-decay-mode-h-bridge-dc-motor-applications/
        #  for more information.
        
        ## Positive duty cycle
        if (duty >= 0):
            if (duty <= 100):
                self.tim2_ch1.pulse_width_percent(100)
                self.tim2_ch2.pulse_width_percent(100 - duty)

            else:
                self.tim2_ch1.pulse_width_percent(100)
                self.tim2_ch2.pulse_width_percent(0)
        
        ## Negative duty cycle
        elif (duty < 0):
            if (duty >= -100):
                self.tim2_ch1.pulse_width_percent(100 + duty)
                self.tim2_ch2.pulse_width_percent(100)
            else:
                self.tim2_ch1.pulse_width_percent(0)
                self.tim2_ch2.pulse_width_percent(100)

def main():
    '''!    @brief  Main program for Project 0x06.
    '''
    ## Motor driver task to run

    ## @brief       Pin objects associated with first motor.
    #  @details     First motor is connected to pins OUT1 and OUT2,
    #               which are commanded by the MCU pins IN1 AND IN2.
    pin_IN1          = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2          = pyb.Pin(pyb.Pin.cpu.B5)
    ## @brief       Pin objects associated with second motor.
    #  @details     Second motor is connected to pins OUT3 and OUT4,
    #               which are commanded by the MCU pins IN3 and IN4.
    pin_IN3          = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4          = pyb.Pin(pyb.Pin.cpu.B1)

    ## Create motor driver object of the Motor class
    motor_drv_1 = Motor(3, 1, 2, pin_IN1, pin_IN2)
    motor_drv_2 = Motor(3, 3, 4, pin_IN3, pin_IN4)

    while True:
        ## Attempt to run FSM unless Ctrl+C is hit
        try:
            motor_drv_1.set_duty(100)
            motor_drv_2.set_duty(20)
        
        ## Break out of loop on Ctrl+C
        except KeyboardInterrupt:
            break

    print("Program terminating...\n"
          "Thank you for using this program.")

if __name__ == '__main__':
    main()