'''!
@file       task_touch_panel_0x06.py
@brief      Implements a touch panel task for the Project 0x06.
@details    Responsible for interacing with the resistive touch
            panel using an object of the touch panel driver class.

            Future versions will perform filtering or averaging
            to improve the quality of data collection and results.

@author     Juan Luna
@date       2021-12-06  Original file
@date       2022-12-22  Modified for portfolio update
'''
from pyb import Pin
import touch_panel
import utime

## Touch panel pin objects
#  "m" stands for minus or negative terminal and
#  "p" for plus or positive terminal

## @brief   Touch panel pin object for y- terminal
pin_ym = Pin(Pin.cpu.A0)
## @brief   Touch panel pin object for x- terminal
pin_xm = Pin(Pin.cpu.A1)
## @brief   Touch panel pin object for y+ terminal
pin_yp = Pin(Pin.cpu.A6)
## @brief   Touch panel pin object for x+ terminal
pin_xp = Pin(Pin.cpu.A7)

class RT_Task:
    '''! @brief      Task implementing touch panel driver functionality.
         @details    Methods of this class will set up touch panel data
                     for data collection, find the speed of the ball by 
                     subtracting current position from the last position.
    '''
    def __init__(self, period, RTP_shares, shares_1, shares_2):
        '''! @brief      Construct future touch panel task objects.
             @details    Instantiates objects responsible for timing tasks,
                         data sharing, and touch panel functionality.

             @param  period      period for touch panel task timing
             @param  RTP_shares  Data sharing variable for IMU sensor
             @param  shares_1    Sharing variable for ball balancing state
             @param  shares_2    Sharing variable for data collection
        '''

        ## Touch-panel-driver-related attributes

        ## Touch panel object of the Touch Panel driver class
        #  for interfacing with the panel.
        self.RTP_drv = touch_panel.Touch_Panel(pin_xm, pin_xp, pin_ym, pin_yp)
        ## Calibrate resistive touch panel at startup
        self.RTP_drv.calibrate()

        ## Timing-related attributes

        ## Period, in microseconds, derived from frequency.
        self.period = period
        ## Counter that sets next time to find speed from position.
        self.next_time = 0

        ## Sharing-related attributes

        ## Object associated with balancing status.
        self.balance = shares_1
        ## Collection data sharing object.
        self.collect = shares_2
        ## Shared data object for touch panel sensor.
        #  Data shared includes x position, y position,
        #  x velocity, y velocity, and "z" scan.
        self.RTP_shares = RTP_shares

    def run(self):
        '''! @brief      Tracks position of ball and computes its speed.   
             @details    Scans current and next position coordinates of 
                         ball when it makes contact with the touch panel.
                         From the last two position data points, speed of
                         the ball is computed.
        '''
        # @brief    List containing x, y, and z components from touch panel. 
        last_state  = (0,0,0)
        
        ## Update timer based on current time stamp
        if (utime.ticks_us() >= self.next_time):

            self.next_time = utime.ticks_us() + self.period

            ## @brief   Location of ball in panel 
            #  @details Output from scanning the position coordinates
            #           of the ball when there is contact.
            xyz_location = self.RTP_drv.scan_all()
            ## @brief   Velocity of ball in panel
            #  @details x and y components of velocity of the ball,
            #           initialized with zeros.
            velocity    = (0,0)

            ## When ball is not in the panel.
            if not xyz_location[2]:
                # If ball is not in contact with the panel,
                # z-scan will return false and we assume that
                # the ball is at the center.
                xyz_location = (0,0, False)

                # Keep record of current position coordinates.
                # Since ball is not in contact, it is assumed to
                # still be at the equilibrium point.
                last_state = (0,0,0)

            ## When ball makes contact with the panel.
            else:
                # If ball makes contact with panel for the first time
                if last_state == (0,0,0):
                    # Scan current position coordinates and 
                    # keep record of them.
                    last_state = xyz_location
                # If ball has been in contact with the panel
                else:
                    # Compute velocity of the ball from last two
                    # positions and timing variables.
                    velocity = (((xyz_location[0]-last_state[0])*self.period),
                                ((xyz_location[1]-last_state[1])*self.period))
                    # Keep record of current position for next computation
                    last_state = xyz_location
            ## Update values of RTP share variable
            self.RTP_shares.write((xyz_location[0], xyz_location[1],
                                velocity[0], velocity[1], xyz_location[2]))