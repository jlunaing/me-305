'''!
@file       touch_panel.py
@brief      A driver for working with a four-wire resistive touch panel.
@details    The touch screen driver, in the form of a class, instantiates
            objects to implement touch screen functionality. Methods of 
            this class will read the position coordinates of the panel at
            the contact point. An additional z component identifies whether
            or not there is contact with the screen. Techniques to improve
            the measurements are considered.

            Methods and functionality of the touch screen class will be 
            implemented in a task that will interface with other tasks that
            control other aspects of the ball-balancing project. 
            
@author     Juan Luna
@date       2021-12-02  Original file
@date       2022-12-22  Modified for portfolio update
'''

from micropython import const
from pyb import ADC, Pin
from ulab import numpy as np
import utime

# Touchscreen physical dimensions (in mm)

## Touchscreen length
length_x = const(176)
## Touchscreen width
width_y = const(100)

## @brief       Maximum value of ADC module
#  @details     The STM32 microcontroller has a 12-bit 3.3V DAC
#               See pg 65 of ME 305 notes
ADC_count       = const(2**12 - 1)

## @brief   Scaling factor for ADC x reading
Kxx         = length_x/ADC_count
## @brief   Shear and rotation factor
Kxy         = 0
## @brief   Shear and rotation factor
Kyx         = 0
## @brief   Scaling factor for ADC y reading
Kyy         = width_y/ADC_count
## @brief   Shifting/offset factor in x direction
x0          = 0
## @brief   Shifting/offset factor in y direction
y0          = 0

class Touch_Panel:
    '''! @brief      Driver class to interface with resistive touch panel.
         @details    Allows future touch panel objects to be instantiated.  
                     Methods of this class will read the x and y coordinates 
                     of the panel representing the location of the contact point. 
                     An additional z component identifies whether or not there 
                     is contact with the screen. To improve the quality of 
                     measurements, settling period and filtering are features
                     we consider.

                     A calibration method is reponsible for calibrating the
                     IMU sensor to ensure accuracy of orientation-related
                     qualities. This, in turn, will ensure correct balancing 
                     of the ball for the Term Project.
    '''
    
    def __init__(self, pin_xm, pin_xp, pin_ym, pin_yp):
        '''! @brief      Instantiates future objects of the touchscreen class.
             @details    Initializes attributes related to pin functionality and
                         parameters representing physical dimensions.
            
             @param  pin_xm  Pin object for - terminal of x resistive element
             @param  pin_xp  Pin object for + terminal of x resistive element
             @param  pin_ym  Pin object for - terminal of y resistive element
             @param  pin_yp  Pin object for + terminal of y resistive element
        '''
        # Pin objects

        ## Pin object for - terminal of x resistive element
        self.pin_XM = pin_xm
        ## Pin object for + terminal of x resistive element
        self.pin_XP = pin_xp
        ## Pin object for - terminal of y resistive element
        self.pin_YM = pin_ym
        ## Pin object for + terminal of y resistive element
        self.pin_YP = pin_yp

        ## Conversion from ADC reading to physical length
        #self.x_adc2mm = length_x/ADC_count
        #self.y_adc2mm = width_y/ADC_count

    def x_scan(self):
        '''!
        @brief      Reads the horizontal "x" location in the touchscreen.
        @details    To scan the x component, we energize the resistor divider
                    between the terminals of the horizontal resistive element.
                    This translates to setting x+ and x- as output pins.

                    The voltage at the center of the divider can be measured by
                    floating (disconnecting from power) the vertical resistive
                    element and measuring the voltage accross the terminals.
                    In code, we would float y+ by setting it as input pin and
                    read the voltage from y- by setting it as analog pin.

                    For accuracy, we account for shifting and scaling of the 
                    actual panel from its idealized version by using linear
                    equation for calibration derived in lecture.

        @return     Horizontal position on the panel from calibration equation.
        '''
        adc = self.xy_scan()
        ## x = Kxx*ADCx   +  Kxy*ADCy   + x0
        return Kxx*adc[0] +  Kxy*adc[1] + x0

    def y_scan(self):
        '''! @brief      Reads the horizontal "y" location in the touchscreen.
             @details    To scan the y component, we energize the resistor divider
                         between the terminals of the vertical resistive element.
                         The voltage at the center of the divider can be measured
                         by floating (disconnecting) the horizontal resistive
                         element and measuring the voltage accross the terminals.

             @return     Vertical position on the panel from calibration equation.
        '''
        adc = self.xy_scan()
        ## y = Kyx*ADCx   +  Kyy*ADCy   + y0
        return Kyx*adc[0] +  Kyy*adc[1] + y0

    def xy_scan(self):
        '''! @brief      Initializes touch panel pin objects for x and y scan.
             @details    Sets up pin objects to read ADC values for x and then
                         re-initializes pin objects to read ADC values for y.
             @return     Tuple containing ADC values from x and y scan.
        '''
        ## X SCAN

        ## Ground x- (set output pin x_m low)
        self.pin_XM.init(mode = Pin.OUT_PP, value = 0)
        ## Power x+ (set output pin x_p high/3.3V)
        self.pin_XP.init(mode = Pin.OUT_PP, value = 1)
        ## Float (disconnect) y+ from ground or 3.3V (set y_p pin as input)
        self.pin_YP.init(mode = Pin.IN)
        ## Measure voltage at center of divider on y- (set y_m pin as analog input) 
        #self.pin_YM.init(mode = Pin.ANALOG)

        #  Create analog object from y_m pin
        ADC_ym = ADC(self.pin_YM)
        #  Read analog value
        ADC_x = ADC_ym.read()

        ## Y SCAN

        ## Ground y- (set output pin y_m low)
        self.pin_YM.init(mode = Pin.OUT_PP, value = 0)
        ## Power y+ (set output pin y_p high/3.3V)
        self.pin_YP.init(mode = Pin.OUT_PP, value = 1)
        ## Float (disconnect) x+ from ground or 3.3V (set x_p pin as input)
        self.pin_XP.init(mode = Pin.IN)
        ## Measure voltage at center of divider on x- (set x_m pin as analog input)
        #self.pin_XM.init(mode = Pin.ANALOG)
        
        #  Create ADC object from x_m pin
        ADC_xm = ADC(self.pin_XM)
        #  Read analog value
        ADC_y = ADC_xm.read()
        
        return (ADC_x, ADC_y)
        
    def z_scan(self):
        '''! @brief      Reads the "z" component of the touchscreen.
             @details    Even though the touch panel is a 2D sensor, we can  
                         consider the two axes to represent whether or not there
                         is contact with the screen. To scan the "z" component,
                         we measure the voltage at thecenter node while both 
                         resistor divider are eenrgized. A way to achieve this is
                         by setting the + terminal of the vertical resistive element
                         HIGH while setting the - terminal of the horizontal
                         resistive element LOW. The voltage of the remaining pins 
                         can be measured to check for contact with the screen.
                        
             @return     Logical value: (1) there is contact, (2) no contact. 
        '''
        
        ## Ground x- (set output pin x_m low)
        self.pin_XM.init(mode = Pin.OUT_PP, value = 0)
        ## Float (disconnect) x+ from ground or 3.3V (set x_p pin as input)
        self.pin_XP.init(mode = Pin.IN)
        ## Power y+ (set output pin y_p high/3.3V)
        self.pin_YP.init(mode = Pin.OUT_PP, value = 1)
        ## Measure voltage at center of divider on y- (set y_m pin as analog input)
        #self.pin_YM.init(mode = Pin.ANALOG)

        #  Create ADC object from y_m pin
        ADC_z = ADC(self.pin_YM)
        
        ## @brief   Account for small offset in ADC reading.
        OFFSET      = 10
        return (ADC_count - OFFSET > ADC_z.read() > 0 + OFFSET)

    def scan_all(self):
        '''! @brief      Reads the x, y, and z readings of the touchscreen.
             @return     x, y, and z scan output values as a tuple.
        '''
        adc = self.xy_scan()

        return (Kxx*adc[0] + Kxy*adc[1] + x0,
                Kyx*adc[0] + Kyy*adc[1] + y0,
                self.z_scan())

    def calibrate(self):
        '''! @brief       Calibrates sensor from pre-existing file or manual calibration.
             @details     Uses manual input to calibrate sensor if no pre-existing 
                          file with calibration coefficients exists. If file exists, 
                          calibration coefficients are read and used.
        '''

        print("Calibrating resistive touch panel...")
        ## @brief   Resistive touch panel calibration data text file name.
        filename = "RT_cal_coeffs.txt"

        # Check if file exists on the Nucleo board's directory.
        # If file exists, read from it.
        try:
            # Read from a file.
            # Using "with" construct with the "open" function makes sure
            # that the file is closed even if an exception occurs.
            # We can open the file with read privileges using 'r' mode.
            with open(filename, 'r') as f:

                # Read first line of the file
                cal_data_string = f.readline()
                # Split line into multiple strings.
                # Then, convert each one to a float
                calib = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                ## Assign calibration values to scaling and offset values
                Kxx   = calib[0]
                Kxy   = calib[1]
                Kyx   = calib[2]
                Kyy   = calib[3]
                x0    = calib[4]
                y0    = calib[5]

        # If file does not exist, calibrate manually and write the 
        # coefficients to the file.
        except:
            print("Welcome to the manual calibration function of my program.\n"
                  "When prompted, touch the panel at the required locations,\n"
                  "using the 20-mm square grids on the screen as reference and\n"
                  "the center of touch screen as the origin (x-0, y=0)")

            ## @brief       Array of calibration points for manual calibration
            #  @details     User will be prompted to touch the screen at the
            #               locations specified by the a series of five 
            #               coordinates. These points correspond to known points
            #               on the calibration grid.
            #
            #               There are five calibration points. This makes a
            #               5x2 matrix of coordinate sample points.
            test_pts        = np.array([[0,0], [80, 40], [-80, 40], [-80, -40], [80, -40]])
            
            ## @brief       A 5x3 array to be populated with ADC calibration readings.
            #  @details     First row of matrix corresponds to ADC readings in x,
            #               the second row holds the ADC readings in y, and the third
            #               row has the z-scan readings. This last row is left 
            #               unmodified with ones as the other rows would only be filled
            #               when the user made contact with the screen. Thus, populating
            #               the other rows means that the z-scans are true (1).
            adc_array       = np.ones((5,3))

            ## Calibrate touch screen with five points.
            for i in range(5):
                print("Make contact with point #{:}: ({:}, {:})".format(i, test_pts[i,0],
                                                                          test_pts[i,1]))
                while True:
                    ## If there is actual contact with the screen.
                    if self.z_scan():
                        ## Scan x and y components
                        xy = self.xy_scan()
                        ## Include x ADC reading in matrix
                        adc_array[i,0] = xy[0]
                        ## Include x ADC reading in matrix
                        adc_array[i,1] = xy[1]

                        while True:
                            if not self.z_scan():
                                break
                        break

            ## Computing 3x2 matrix of calibration coefficients
            
            ## @brief       Best approximation of 3x2 matrix of calibration coefficients
            #  @details     Using NumPy package in Python, we perform matrix math to
            #               find the calibration coefficients derived in lecture.
            #
            #               The desired x and y coordinates (outputs) are linear
            #               combinations of the ADC measurements, transformed (or scaled)
            #               by our K coefficients. These linear combinations are offset
            #               by x0 and y0.
            #               As such, we can define our output vector [x y] and express
            #               it in matrix form. Using linear algebra, we can solve for the 
            #               K coefficients. 

            ## Take the inverse of the dot product (c = a.b) of the transpose of the array
            #  of ADC readings (a) and the ADC readings (b).
            #  Then, take the dot product (d = c.a) of the inverse from before (c) and
            #  the inverse of the ADC reading matrix from before (a).
            #  Finally, take the dot product (e = d.f) of the result from before (d) and
            #  the array of calibration coordinates (f). 
            cal_coeffs = np.dot(np.dot(np.linalg.inv(np.dot(adc_array.transpose(), (adc_array))),
                                                (adc_array.transpose())), (test_pts))
            
            ## Assign calibration coefficients to individual variables
            Kxx = cal_coeffs[0,0]
            Kyx = cal_coeffs[0,1]
            Kxy = cal_coeffs[1,0]
            Kyy = cal_coeffs[1,1]
            x0  = cal_coeffs[2,0]
            y0  = cal_coeffs[2,1]
            
            # Writing the calibration data back to a file requires
            # to open the file in write mode with 'w'.
            with open(filename, 'w') as f:
                # Write the calibration coefficients to the file
                # as a string. The example uses an f-string, but we can
                # use string.format() as well.
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {x0}, {y0}\r\n")

        print("\nKxx: {:}, \tKyx: {:}\n"
              "Kxy: {:}, \tKyy: {:}\n"
              "Xc:  {:}, \tYc:  {:}\n".format(Kxx, Kyx, Kxy, Kyy, x0, y0))

        print("The touch panel has been calibrated!")

def main():
    '''! @brief     Main test program for touch panel driver
    '''

    ## Touchscreen pin objects
    #  "m" stands for minus or negative terminal and
    #  "p" for plus or positive terminal
    pin_ym = Pin(Pin.cpu.A0)
    pin_xm = Pin(Pin.cpu.A1)
    pin_yp = Pin(Pin.cpu.A6)
    pin_xp = Pin(Pin.cpu.A7)

    ## Touch panel driver tasks to calibrate and run
    rp_driver = Touch_Panel(pin_xm, pin_xp, pin_ym, pin_yp)
    rp_driver.calibrate()

    while True:
        ## Attempt to run FSM unless Ctrl+C is hit
        try:
            ## If there is contact with the screen
            if rp_driver.z_scan():
                ## Start timer
                t0 = utime.ticks_us()
                ## Scan x, y, and z components
                rp_driver.scan_all()
                ## Stop timer and compute duration
                dt = utime.ticks_diff(utime.ticks_us(), t0)

                ## Duration for all three scans
                print(str(dt) + " us")
                ## Position on the screen, in mm
                print("X: {:},\t Y: {:}".format(round(rp_driver.x_scan()),
                                                round(rp_driver.y_scan())))
            
        ## Break out of loop on Ctrl+C
        except KeyboardInterrupt:
            break
        
    print("Program terminating...\n"
          "Thank you for using this program.")

if __name__ == '__main__':
    main()