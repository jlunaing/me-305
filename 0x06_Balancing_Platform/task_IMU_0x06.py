'''!
@file       task_IMU_0x06.py
@brief      This module implements BNO055 sensor driver functionality.
@details    Responsible for interfacing with the BNO055 IMU sensor 
            attached to the top platform using an object from the 
            associated IMU driver class.
            
@author     Juan Luna
@date       2021-12-05  Original file
@date       2022-12-22  Modified for portfolio update
'''

from pyb import I2C
import BNO055_0x06
import os
import utime

class Task_IMU:
    '''! @brief      IMU task implementing BNO055 driver functionality.
    '''
    
    def __init__(self, period, IMU_shares, shares_1, shares_2):
        '''! @brief              Constructs future IMU task objects
             @details            Instantiates objects responsible for serial
                                 communication, driver for incorporating IMU
                                 functionality, and sets up calibration on
                                 startup.

             @param  period      period for IMU task timing
             @param  IMU_shares  Data sharing variable for IMU sensor
             @param  shares_1    Data sharing variable for ball balancing state
             @param  shares_2    Data sharing variable for data collection
        '''
        ## BNO055-driver-related attributes 

        ## @brief   Create I2C object attached to bus 1.
        #  @details Bus is initialized in controller/master mode
        i2c = I2C(1, I2C.MASTER)

        ## Initialize I2C bus with following parameters:
        #  MODE: master/controller
        #  BAUDRATE: SCL clock rate
        i2c.init(I2C.MASTER, baudrate = 400_000)

        ## Create IMU driver object with following parameters:
        # ADDR: I2C address is 0x28
        # I2C: I2C object
        self.imu_drv = BNO055_0x06.BNO055(0x28, i2c)
        ## IMU sensor calibration at startup
        self.imu_drv.calibrate()

        ## Timing-related attributes

        ## Period for timing IMU task, defined in main program
        self.period = period
        ## Time stamp when timer reaches next period
        self.next_time = 0

        ## Sharing-related attributes

        ## Object associated with balancing status.
        self.balance = shares_1
        ## Collection data sharing object.
        self.collect = shares_2
        ## Shared data object for IMU
        #  Data shared includes roll (theta_y), pitch (theta_x),
        #  change in roll (omega_y), and change in pitch (omega_x)   
        self.IMU_shares = IMU_shares
                     
    def run(self):
        '''! @brief      Runs IMU task implementing driver functionality.
             @details    Share variable is updated with Euler angles and
                         velocity values when data collection occurs.
        '''
        
        ## Update timer based on current time stamp
        if (utime.ticks_us() >= self.next_time):

            self.next_time = utime.ticks_us() + self.period

            ## Read Euler angles from IMU sensor
            angles = self.imu_drv.read_euler_angles()
            ## Read angular velocity from IMU sensor
            velocities = self.imu_drv.read_velocity()

            ## Update values of IMU share variable
            self.IMU_shares.write((angles[1], angles[2],
                                  velocities[0] , velocities[1]))