'''! @file       task_user_0x06.py
     @brief      This module implements user interface for Project 0x06.
     @details    Implements a simple user interface and facilitates interaction
                 with encoder and motor objects. This interaction takes place in
                 "main.py". Specifically, this file, as well as "task_encoder.py"
                 and "task_motor.py" are imported as modules.

                 This task handles all serial communication between user and
                 backend running on Nucleo. It accepts a lists of user commands.       
    
     @author     Juan Luna
     @date       2021-12-08  Original file
     @date       2022-12-22  Modified for portfolio update
'''

import array
import pyb
import utime

## State 0 variable, Initializing state
S0_INIT = 0
## State 1 variable, Waiting for the user to input a key command
S1_WAIT_FOR_INPUT = 1
## State 2 variable for prompting proportional gain
S2_PROMPT         = 2
## State 3 variable for prompting integral gain
S3_TIME_DELAY     = 3

class Task_User:
    '''! @brief          "User Task" class for user interface task.
         @details        Facilitates interaction between user running Putty and
                         Nucleo board using a user interface. This interface
                         gives a list of available commands and prompts user to
                         enter their desired command. 
    '''

    def __init__(self, period, IMU_queue, RTP_queue, motor_queue,
                shares_1, shares_2, stop_shares, timer_shares):
        '''! @brief          Construct future user task objects
             @details        Instantiates attributes related to communication
                             over virtual serial port, timing as it relates to
                             user interface and data (i.e., position or delta
                             values) print statements, and similar.
                @param  period      Period of task, specified in main script
                @param      IMU_queue       Queue variable for IMU data
                @param      RTP_queue       Queue variable for touch panel data
                @param      motor_queue     Queue variable for motor data
                @param      shares_1        Share variable for first motor
                @param      shares_2        Share variable for second motor
                @param      stop_shares     Stop data collection
                @param      timer_shares    Start time share variable


        '''
        ## Counter tracking the number of times the task has run.
        self.runs = 0 
        ## Period is set as the value specified in main script.
        self.period = period
        ## Time stamp
        self.current_time = utime.ticks_us()
        ## Time is updated once clock reaches period plus the current time
        self.next_time = 0

        ## VCP object to handle serial communication over USB.
        #  Receive characters user types in command prompt, as they 
        #  are received, using this VCP. In this way, it facilitates
        #  communication between user running PuTTY and Nucleo board.
        self.comm_reader = pyb.USB_VCP()

        ## Queue variable for BNO055 IMU sensor.
        #  Data shared includes roll (theta_y), pitch (theta_x),
        #  change in roll (omega_y), and change in pitch (omega_x)   
        self.IMU_queue = IMU_queue

        ## Queue variable for touch panel sensor.
        #  Data shared includes x position, y position,
        #  x velocity, y velocity, and "z" scan.
        self.RTP_queue = RTP_queue

        ## Queue variable for timing duty cycle.
        #  Data shared includes time and duty cycles.
        self.motor_queue = motor_queue

        ## Object associated with balancing status.
        self.balance = shares_1
        ## Collection data sharing object.
        self.collect = shares_2
        
        ## End data collection shares object.
        self.stop_data = stop_shares
        ## Timing shares object.
        self.start_time = timer_shares

        ## The state to run on the next iteration of the task.
        self.state = S0_INIT

        ## Array for time data collection
        self.time_array     = array.array('d', [])

        ## Array for motor 1 PWM data
        self.M1_array       = array.array('i', [])
        ## Array for motor 2 PWM data
        self.M2_array       = array.array('i', [])

        ## Array for touch panel x position data
        self.x_array        = array.array('d', [])
        ## Array for touch panel y position data
        self.y_array        = array.array('d', [])
        ## Array for touch panel x velocity data

        self.x_dot_array    = array.array('d', [])
        ## Array for touch panel y velocity data
        self.y_dot_array    = array.array('d', [])
        ## Array for IMU angular x position data
        self.theta_x_array  = array.array('d', [])
        ## Array for IMU angular y position data
        self.theta_y_array  = array.array('d', [])
        ## Array for IMU angular x velocity data
        self.omega_x_array  = array.array('d', [])
        ## Array for IMU angular y position data
        self.omega_y_array  = array.array('d', [])

    def run(self):
        '''! @brief      Run relevant functions required by user task.
             @details    Transitions through states of the FSM and displays
                         a user interface at initialization state. Waits for
                         user input when necessary.
        '''

        ## Update timer based on current time stamp
        if utime.ticks_us() >= self.next_time:

            self.next_time = utime.ticks_us() + self.period
            ## Initialization state
            if self.state == S0_INIT:
                ## Display list of commands
                print("\033c", end = "")
                print(" -----------------------------------------------------\n"
                      " USER COMMANDS FOR BALL-BALANCING PLATFORM:\n"
                      "\tb:\tStart balancing ball on platform\n"
                      "\tm:\tStop balancing\n"
                      "\tg:\tStart collecting touch panel and IMU data\n"
                      "\ts:\tStop collecting touch panel and IMU data\n"
                      "\tq:\tShow list of command again.\n\n"
                      "Enter a command...\n")

                ## Transition to state 1
                self.state = S1_WAIT_FOR_INPUT
            
            ## State 1 logic
            elif self.state == S1_WAIT_FOR_INPUT:
                
                if self.collect.read():
                    
                    ## While there is at least one item in the queue.
                    while self.RTP_queue.num_in() > 0:

                        ## Remove first item from queue and return its value
                        #  to assigned variable 
                        rtp_data = self.RTP_queue.get()
                        imu_data = self.IMU_queue.get()
                        duty_data = self.motor_queue.get()

                        ## Append timing data to array
                        self.time_array.append(duty_data[0])

                        ## Append position and velocities to arrays
                        self.x_array.append(rtp_data[0])
                        self.y_array.append(rtp_data[1])

                        self.x_dot_array.append(rtp_data[2])
                        self.y_dot_array.append(rtp_data[3])
                        
                        self.theta_x_array.append(imu_data[0])
                        self.theta_y_array.append(imu_data[1])
                        self.omega_x_array.append(imu_data[2])
                        self.omega_y_array.append(imu_data[3])
                        
                        ## Append motor duty cycles to array
                        self.M1_array.append(duty_data[1])
                        self.M2_array.append(duty_data[2])

                elif self.stop_data.read():
                    ## Update value of shared variables
                    self.collect.write(0)
                    self.stop_data.write(0)
                    ## Display data on screen
                    self.print_data()

                # waiting for user input state
                if (self.comm_reader.any()):
                    ## Reads user input from serial port
                    user_input = self.comm_reader.read(1).decode()

                    ## Logic of commands

                    #  Stop ball balancing
                    if user_input == 'm':                        
                        self.balancing.write(0)
                        print("Stopped ball balancing.")

                    elif user_input == 'b':
                        self.balancing.write(1)
                        print("Ball balancing started.")

                    elif self.collect.read() == 0 and user_input == 'g':
                        
                        print("Collecting data at {:02d}:{:02d}:{:02d}"
                            '\n'.format(utime.localtime()[3], utime.localtime()[4], utime.localtime()[5]), end='')
                        self.collect.write(1)
                        
                        ## Empty arrays and queues
                        self.clear_containers()
                        ## Start timer
                        self.start_time.write(utime.ticks_us())

                    ## Stop data collection
                    elif self.collect.read() == 1 and user_input == 's':
                        self.collect.write(0)
                        ## Print collected data
                        self.print_data()

                    ## Transition to state 0 to display commands again
                    elif user_input == 'q':
                        self.state = S0_INIT

    def print_data(self):
        '''! @brief      Prints data collected from arrays.
             @details    Displays linear and angular positions found from
                         ball-balancing data collection.
        '''

        print("\ntime (s)\tX (mm),\tY (mm),\tX_vel (mm/s),\tY_vel (mm/s),\t"
            "theta_X(rad),\tTheta_Y (rad),\tOmega_X (rad/s),\tOmega_Y (rad/s)")

        for i in range(0, len(self.time_array)):

            print("{:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}"
                  .format(round(self.time_array[i] / 1E6, 3),
                    round(self.x_array[i], 2), round(self.y_array[i], 2),
                    round(self.x_dot_array[i], 2), round(self.y_dot_array[i], 2),
                    round(self.theta_x_array[i], 2), round(self.theta_y_array[i], 2),
                    round(self.omega_x_array[i], 2), round(self.omega_y_array[i], 2)))

        print("Collection ended at {:02d}:{:02d}:{:02d}"
              '\n'.format(utime.localtime()[3], utime.localtime()[4], utime.localtime()[5]), end='')

    def clear_containers(self):
        '''! @brief      Empty queues and arrays.
             @details    Clears queues and re-initializes arrays for next
                         data collection step.     
        '''

        ## Remove first item in data sharing list
        #  and continue for as long as there is items.
        while self.RTP_queue.num_in():
            self.RTP_queue.get()

        while self.IMU_queue.num_in():
            self.IMU_queue.get()
            
        while self.motor_queue.num_in():
            self.motor_queue.get()
        
        ## Empty arrays for next data collection.
        self.time_array = array.array('d', [])
        self.x_array = array.array('d', [])

        self.y_array = array.array('d', [])
        self.x_dot_array = array.array('d', [])
        self.y_dot_array = array.array('d', [])

        self.theta_x_array = array.array('d', [])
        self.theta_y_array = array.array('d', [])
        self.omega_x_array = array.array('d', [])
        self.omega_y_array = array.array('d', [])

        self.M1_array = array.array('f', [])
        self.M2_array = array.array('f', [])