'''!
@file       task_controller_0x06.py
@brief      A driver for implementing closed-loop control.
@details    Responsible for implementing closed-loop full-state
            control feedback with additional features to improve
            performance of balancing platform. This task also
            interfaces with the motors through implementation of
            motor driver functionality to deliver the correct
            PWM signal associated with the actuation values 
            computed in the closed-loop control algorithm.

@author     Juan Luna
@date       2021-12-07  Original file
@date       2022-12-22  Modified for portfolio update
'''

from micropython import const
from ulab import numpy as np
from pyb import Pin
import motor_0x06
import utime

# Motor pins

## @brief       First MCU control pin associated with first motor.
#  @details     First motor is connected to pins OUT1 and OUT2,
#               which are commanded by the MCU pins IN1 AND IN2.
pin_IN1         = Pin(Pin.cpu.B4)
## @brief       Second MCU control pin associated with first motor.
#  @details     First motor is connected to pins OUT1 and OUT2,
#               which are commanded by the MCU pins IN1 AND IN2.
pin_IN2         = Pin(Pin.cpu.B5)
## @brief       First MCU control pin associated with second motor.
#  @details     Second motor is connected to pins OUT3 and OUT4,
#               which are commanded by the MCU pins IN3 and IN4.
pin_IN3         = Pin(Pin.cpu.B0)
## @brief       Second MCU control pin associated with second motor.
#  @details     Second motor is connected to pins OUT3 and OUT4,
#               which are commanded by the MCU pins IN3 and IN4.
pin_IN4         = Pin(Pin.cpu.B1)

# Relevant constants

## DC voltage value that powers ball-balacing system.
V_DC = const(12)
## Terminal resistance of DCX22S motor. in units of ohms.
R_OHM = const(2.21)
## Torque constant of DCX22S motor, in units of N-m/A.
K_T = const(0.0138)

class Task_Control:
    '''!    @brief      Task implementing motor control functionality.
            @details    Methods of this class set up variables reponsible
                        for interfacing with motors and data sharing from
                        touch panel and IMU tasks.
    '''
    
    def __init__(self, motor_period, collection_period, IMU_shares, IMU_queue,
                RTP_shares, RTP_queue, shares_1, shares_2, stop_shares,
                timer_shares, motor_queue):        
        '''!
        @brief      Constructs future objects of the Task Control class.
        @details    Instantiates objects responsible for timing tasks,
                    motor control, data sharing, and closed-loop control.

        @param      motor_period        Period of the motor
        @param      collection_period   Period for collection time
        @param      IMU_shares          Share variable for IMU data
        @param      IMU_queue           Queue variable for IMU data
        @param      RTP_shares          Share variable for touch panel data
        @param      RTP_queue           Queue variable for touch panel data
        @param      shares_1            Share variable for first motor
        @param      shares_2            Share variable for second motor
        @param      stop_shares         Stop data collection
        @param      timer_shares        Start time share variable
        @param      motor_queue         Queue variable for motor data
        '''
        
        # Motor-driver-related attributes

        ## First motor driver object
        self.motor_drv_1 = motor_0x06.Motor(3, 1, 2, pin_IN1, pin_IN2)
        ## Second motor driver object
        self.motor_drv_2 = motor_0x06.Motor(3, 3, 4, pin_IN3, pin_IN4)

        # Timing-related attributes

        ## Period, in microseconds, derived from frequency.
        self.motor_period = motor_period
        ## Collection period for data collection. 
        self.collection_period = collection_period
        ## Counter that sets next time to find speed from position.
        self.next_time = 0
        ## Next data collection time       
        self.next_collect = 0
        ## Time elapsed
        self.delta_time = 0
        
        ## Sharing-related attributes

        ## Shared data object for IMU
        #  Data shared includes roll (theta_y), pitch (theta_x),
        #  change in roll (omega_y), and change in pitch (omega_x)   
        
        ## Share variable for BNO055 IMU sensor.
        self.IMU_shares = IMU_shares
        ## Queue variable for BNO055 IMU sensor.
        self.IMU_queue = IMU_queue

        ## Shared data object for touch panel sensor.
        #  Data shared includes x position, y position,
        #  x velocity, y velocity, and "z" scan.
        self.RTP_shares = RTP_shares
        ## Queue variable for touch panel sensor.
        self.RTP_queue = RTP_queue

        ## Queue variable for timing duty cycle.
        #  Data shared includes time and duty cycles.
        self.motor_queue = motor_queue

        ## Object associated with balancing status.
        self.balance = shares_1
        ## Collection data sharing object.
        self.collect = shares_2
        ## End data collection shares object.
        self.stop_data = stop_shares
        ## Timing shares object.
        self.start_time = timer_shares

        # Duty cycles to produced needed torque to balance ball are initialized.
        ## Duty cycle of first motor.
        self.duty_M1 = 0
        ## Duty cycle of second motor.
        self.duty_M2 = 0

    def collect_data(self):
        '''! @brief      Reads hardware shared data and writes it to queues.
             @details    Touch screen, IMU sensor, and motor data shared
                         is added to each respective queue.
        ''' 
        
        # Add touch panel position coordinates data to RTP queue.
        self.RTP_queue.put(self.RTP_shares.read())
        # Add IMU orientation data to IMU queue.
        self.IMU_queue.put(self.IMU_shares.read())
        ## Add duty cycle and timing data to motor queue.
        self.motor_queue.put((self.delta_time, self.duty1, self.duty2))
        
    def run(self):       
        '''! @brief      Runs algorithm for closed-loop control
             @details    Method updates gain and speed values. Prints
                         duty cycles of motors as well as angular positions
                         and angular velocities of the platform. 
        '''

        ## Update timer based on current time stamp
        if (utime.ticks_us() >= self.next_time):

            self.next_time = utime.ticks_us() + self.motor_period
            
            ## If ball is in the platform and balancing status is true,
            #  then we set the duty cycle required to produce the torque Tx
            #  needed for proper balancing.
            if self.balance.read():
                ## Keep track of time
                self.delta_time = utime.ticks_diff(utime.ticks_us(),
                                                    self.start_time.read())

                ## Access values of touch panel and IMU shared variables
                #  Read x and y positions and velocities
                xy_data = self.RTP_shares.read()
                #  Read orientation angles and velocities
                euler_data = self.IMU_shares.read()

                ## Construct state vector containing the dynamics
                #  of the ball and the platform. Information regarding
                #  the dynamics of the ball is given by the touch panel
                #  output (x and y positions and velocities) while the 
                #  dynamics of the platform is encapsulated in IMU 
                #  output (theta_x, theta_y, omega_x, and omega_y)
                state_vec_1 = np.array([[xy_data[0]/1000], [euler_data[1]],
                                    [xy_data[2]/1000], [euler_data[2]]])  
                
                state_vec_2 = np.array([[xy_data[1]/1000], [euler_data[0]],
                                    [xy_data[3]/1000], [euler_data[3]]])
                
                ## Construct array of system gains for full-state feedback
                #  Units of N, N-m, N-s, and N-m-s
                K1_gains = np.array([ 4.5, -1.8,  0.5, -0.1])
                K2_gains = np.array([-4.5, -1.8, -0.5,  0.1])
                
                ## Required gain to get duty cycle required to
                #  produce needed torque Tx
                PWM_gain = R_OHM*100/(4*K_T*V_DC)

                ## Set duty cycle for proper ball-balancing.
                self.duty_M1 = np.dot(K1_gains, state_vec_1)[0]*PWM_gain
                self.duty_M2 = np.dot(K2_gains, state_vec_2)[0]*PWM_gain

                self.motor_drv_1.set_duty(self.duty_M1)
                self.motor_drv_2.set_duty(self.duty_M2)

                ## Print dynamics data on screen
                print(f"X:{xy_data[0]} Y:{xy_data[1]}")
                print(f"Vx:{xy_data[2]} Vy:{xy_data[3]}")
                print(f"theta(x):{euler_data[1]} theta(x):{euler_data[0]}")
                print(f"omega(x):{euler_data[3]} omega(y):{euler_data[2]}")

            ## If the ball is not on the platform, 
            #  stop the motors by zeroing the duty cycles.
            else:
                self.motor_drv_1.set_duty(0)
                self.motor_drv_2.set_duty(0)

            ## Collect data when user selects appropriate command
            if (utime.ticks_us() >= self.next_collect) and self.collect.read() and self.stop_data.read() == 0:
                ## Update timer based on current time stamp
                self.next_collect = utime.ticks_us() + self.collection_period
                ## Read touch panel, IMU, and motor shared data
                self.collect_data()