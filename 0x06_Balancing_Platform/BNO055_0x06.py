'''!    @file       BNO055_0x06.py
        @brief      A driver class for interfacing with the BNO055 sensor.

        @details    Driver that encapsulates functionality of the BNO055
                    inertial measurement unit (IMU) sensor. It will be 
                    implemented in the form of a task that will interact
                    cooperatively with other tasks for the Term Project.

        @author     Juan Luna
        @date       2021-12-07  Original file
        @date       2022-12-22  Modified for portfolio update
'''

from pyb import I2C
import utime

## Calibration status
CALIB_STAT      = 0x35
## Calibration coefficient, up to 0x6A
CALIB_START     = 0x55
## Euler angle data registries, up to 0x1F
EULER_START     = 0x1A
## Gyroscope data registries, up to 0x19
GYR_START       = 0x14
## NDOF mode offers all three sensor outputs and fuses the data using
#  absolute orientation (See Table 3-3, 3-5 in datasheet)
NDOF_MODE       = 0x0C

# Buffer length

## Buffer length for status
status_size     = 1
## Buffer length for coefficients
coeff_size      = 22
## Buffer length for euler angles
angles_size     = 6
## Buffer length for velocity
vel_size        = 6

class BNO055:
    '''! @brief      A driver class for the BNO055 IMU sensor.
         @details    Encapsulates funcionality of BNO055 sensor. This driver
                     instantiates objects and defines methods that collectively
                     allow program to obtain orientation information in the
                     form of Euler angles (heading, pitch, and roll) and 
                     angular velocity data from gyroscopic data.

                     A calibration method is responsible for calibrating the
                     resistive touch panel to improve accuracy by accounting
                     for discrepancies between ideal and actual physical panels. 
    '''
    def __init__(self, address, i2c):
        '''! @brief      Initializes future objects of the BNO055 class.
             @details    Objects of the BNO055 class are related to I2C
                         serial communication and calibration.
        '''
        ## @brief   I2C device address defined in IMU task code
        self.address = address
        ## @brief   I2C device object defined in IMU task code
        self.i2c = i2c
        ## 9-DOF operating mode
        self.set_mode(NDOF_MODE)

        # Buffer bytearrays

        ## Buffer length for status
        self.buf_status = bytearray(status_size)
        ## Buffer length for coefficients
        self.buf_coeff  = bytearray(coeff_size)
        ## Buffer length for euler angles
        self.buf_angles = bytearray(angles_size)
        ## Buffer length for velocity
        self.buf_vels   = bytearray(vel_size)

    def turn_off(self):
        '''! @brief      Turn off I2C bus when exiting program
        '''
        self.i2c.deinit()

    def set_mode(self, data):
        '''! @brief      Changes operating mode of the IMU to 9DOF mode.
             @details    Based on user input, writes operating mode to IMU
                         register address.
        '''
        ## OPERATING_MODE = 0x3D
        self.i2c.mem_write(data, self.address, 0x3D)
    
    def get_calib_stat(self):
        '''! @brief      Gets calibration status of the IMU
             @return     Calibration status
        '''
        ## CALIB_STAT = 0x35
        self.i2c.mem_read(self.buf_status, self.address, CALIB_STAT)
        
        return self.buf_status[0]
        
    def get_calib_coeff(self):
        '''! @brief      Reads calibration coefficients from IMU as binary data
             @return     Calibration coefficients
        '''
        ## CALIB_START: 0x55 to 0x6A
        self.i2c.mem_read(self.buf_coeff, self.address, CALIB_START)
        ## Unpack binary data
        return self.buf_coeff
        
    def write_calib_coeff(self, cal_coeffs):
        '''! @brief      Writes calibration coefficients from the IMU.
        '''
        ## CALIB_START: 0x55 to 0x6A
        self.i2c.mem_write(cal_coeffs, self.address, CALIB_START)
    
    def read_euler_angles(self):
        '''! @brief      Reads Euler angles from the IMU.
             @details    Reads serial data (over I2C) from IMU and returns 
                         heading, pitch, and roll values as a tuple.
             @return     Tuple of Euler angles in rad.   
        '''
        ## EULER_START: 0x1A to 0x1F 
        self.i2c.mem_read(self.buf_angles, self.address, EULER_START)

        ## @brief   List containing Euler angles for all directions                        
        #  @details Angular positions for roll (index 0),
        #           pitch (index 1), and heading (index 3).
        euler_angles = [0,0,0]

        ## Unpacking binary data in "manual" mode

        # Combine bytes with bitshifts and the "OR" operator
        euler_angles[0] = self.buf_angles[0] | self.buf_angles[1] << 8
        euler_angles[1] = self.buf_angles[2] | self.buf_angles[3] << 8
        euler_angles[2] = self.buf_angles[4] | self.buf_angles[5] << 8

        # Sign extension: Convert unsigned value to signed.
        for i in range(3):
            # If the magnitude is greater than half the range (32,767),
            # subtract full range (65,536).
            if euler_angles[i] > 32767:
                euler_angles[i] -= 65536
                
        return (euler_angles[0]/900, euler_angles[1]/900, euler_angles[2]/900)      
        
    def read_velocity(self):
        '''! @brief      Reads angular velocity from the IMU.
             @return     Tuple of velocity values in rad/s for each direction.
        '''
        ## GYR_START: 0x14 to 0x19
        self.i2c.mem_read(self.buf_vels, self.address, GYR_START)

        ## @brief   List containing velocities for all directions                        
        #  @details Angular velocities for roll (index 0),
        #           pitch (index 1), and heading (index 3). 
        vel_values  = [0,0,0]

        ## Unpacking binary data in "manual" mode

        # Combine bytes with bitshifts and the "OR" operator
        vel_values[0] = self.buf_vels[0] | self.buf_vels[1] << 8
        vel_values[1] = self.buf_vels[2] | self.buf_vels[3] << 8 
        vel_values[2] = self.buf_vels[4] | self.buf_vels[5] << 8
        
        # Sign extension: Convert unsigned value to signed.
        for i in range(3):
            # If the magnitude is greater than half the range (32,767),
            # subtract full range (65,536).
            if vel_values[i] > 32_767:
                vel_values[i] -= 65_536
        
        return (vel_values[0]/900, vel_values[1]/900, vel_values[2]/900)

    def print_cal(self):
        '''! @brief       Calibration status for each IMU sensor.
             @details     Prints calibration status for the system,
                          gyroscope, accelerometer, and magnetometer.
                          An output of 255 means fully calibrated.

             @return      Calibration status
        '''
        
        cal_output = [False, False, False, False]

        SYS = 0b11000000
        GYR = 0b00110000
        
        ACC = 0b00001100
        MAG = 0b00000011

        if (self.buf_status[0] & SYS) == SYS:
            cal_output[0] = True
        if (self.buf_status[0] & GYR) == GYR:
            cal_output[1] = True

        if (self.buf_status[0] & ACC) == ACC:
            cal_output[2] = True
        if (self.buf_status[0] & MAG) == MAG:
            cal_output[3] = True

        return cal_output

    def calibrate(self):
        '''! @brief       Reads and writes calibration data on startup.
             @details     If file with calibration coefficients already exists,
                          calibration coefficients are read and used for 
                          calibration. Otherwise, if no pre-existing file 
                          exists, manual input is used to calibrate sensor.
                        
                          Method writes IMU calibration data to STM32
                          Nucleo's built-in flash storage "PYBFLASH".
        '''
           
        print("Calibrating BNO055 IMU sensor...")
        ## @brief   IMU sensor calibration data text file name.
        filename = "IMU_cal_coeffs.txt"

        ## Change back to 9-DOF operating mode
        self.change_mode(NDOF_MODE)

        # Set up correct mode
        mode = self.i2c.mem_read(1, self.address, 0x3D)
        # Change mode to calibration mode 
        self.i2c.mem_write(0, self.address, 0x3D)
        # Calibratio mode 59
        self.i2c.mem_write(6, self.address, 0x3B)
        # Calibration mode 61
        self.i2c.mem_write(mode, self.address, 0x3D)

        # Check if file exists on the Nucleo board's directory.
        # If file exists, read from it.
        try:
             # Read from a file.
            # Using "with" construct with the "open" function makes sure
            # that the file is closed even if an exception occurs.
            # We can open the file with read privileges using 'r' mode.
            with open(filename, 'r') as f:

                # Read first line of the file
                cal_values = f.readline()
                # Split line into multiple strings.
                # Then, convert each one to a float
                # Coefficients is 22-byte long bytearray
                calib = cal_values.strip().split(",")

                byte_arr = bytearray(coeff_size)
                for i in range(coeff_size):
                    byte_arr[i] = int(calib[i], 16)
                self.write_calib_coeff(byte_arr)

        # If file does not exist, calibrate manually and write the 
        # coefficients to the file.
        except:
            print("Welcome. To calibrate IMU, please move platform\n"
                  "to different orientations. Keep it at that position\n"
                  "momentarily and then change. Feel free to move panel\n"
                  "however you want...\n")

            print("[SYS,  GYR,  ACC,  MAG]")
            next_time = utime.ticks_us() + 1E6

            while True:
                
                if(utime.ticks_us() >= next_time):
                    print(str(self.print_cal()) + "\t" +
                          str(self.get_calib_stat()))

                    next_time = utime.ticks_us() + 1E6

                if (self.get_calib_stat() == 0b11111111):
                    self.write_calib_coeff(self.get_calib_coeff())
                    cal_values = ""

                    for coeff in self.buf_coeff:
                        cal_values += hex(coeff) + ", "

                    cal_values = cal_values[:-2]

                    # Writing the calibration data back to a file requires
                    # to open the file in write mode with 'w'.  
                    with open(filename, 'w') as f:
                        # Write the calibration coefficients to the file
                        # as a string. The example uses an f-string, but
                        # we can use string.format() as well.
                        f.write(f"{cal_values}\r\n")
                    break

        print("The BNO055 IMU sensor has been calibrated.")
        print(cal_values)

def main():
    '''!    @brief  Main program for testing BNO055 in Project 0x06.
    '''
    ## IMU driver task to calibrate and run

    ## @brief   Create I2C object attached to bus 1.
    #  @details Bus is initialized in controller/master mode
    i2c = I2C(1, I2C.MASTER)

    ## Initialize I2C bus with following parameters:
    #  MODE: master/controller
    #  BAUDRATE: SCL clock rate
    i2c.init(I2C.MASTER, baudrate = 400_000)

    ## Create IMU driver object with following parameters:
    # ADDR: I2C address is 0x28
    # I2C: I2C object
    imu_driver = BNO055(0x028, i2c)

    ## Calibrate IMU sensor
    imu_driver.calibrate()

    while True:
        ## Attempt to run FSM unless Ctrl+C is hit
        try:
            print(imu_driver.read_euler_angles())
            print(imu_driver.read_velocity())
            ## Momentary pause between readings.
            utime.sleep(1)
        
        ## Break out of loop on Ctrl+C
        except KeyboardInterrupt:
            break

    print("Program terminating...\n"
          "Thank you for using this program.")

if __name__ == '__main__':
    main()