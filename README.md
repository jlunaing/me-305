# Mechatronics Portfolio

This repository contains the source code for programs that interface the 
STM32L476RG Nucleo microcontroller with a variety of sensors and actuators.

## Overview

The code is written in Python (MicroPython) and structured using drivers, that
capture the functionality of the particular device, and tasks, that implement 
the driver code and run cooperatively with each other to accomplish the desired
behavior. The drivers can be repurposed in other Python programs that use 
similar hardware.

Here is a brief description of the directories:

* `Pushbutton_Controlled_LED`: LED-brightness control using pushbuttons.
* `Encoder_Control`: Motor shaft position and velocity measurement with incremental encoders.
* `DC_Motor_Control`: DC motor control using incremental encoder. 
* `Closed_Loop_Control`: Closed-loop PID control of a DC motor.
* `I2C_with_IMU`: Absolute orientation measurement using IMU communicating over I2C.
* `Balancing_Platform`: A system that balances a ball atop of a platform.  

## Documentation

For more details, documentation of all source files are available in my 
[portfolio website](https://jlunaing.bitbucket.io/).