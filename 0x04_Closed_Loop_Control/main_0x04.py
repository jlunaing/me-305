'''!
@file       main_0x04.py
@brief      Main script for overall execution of Project 0x04: Closed-Loop Control
@details    Imports @c task_encoder_0x04.py, @c task_user_0x04.py, and 
            @c task_motor_0x04.py modules to perform closed-loop speed control
            on the shaft of a DC motor. A step response is carried out in this
            program and a user interface displays relevant motor information and
            gives user ability to change PID controller values, stop the
            response manually, and see the data printed on the terminal.

            More details in 
            <a href = "https://bitbucket.org/jlunaing/me-305/src/master/0x04_Closed_Loop_Control/">
            source code</a> and @ref writeup_0x04_controller "documentation".

            State-transition diagram for FSM:

            @image  html    0x04_fsm.png "" width = 85%
            
@author     Juan Luna
@date       2021-11-13  Original file
@date       2022-12-22  Modified for portfolio update
'''

import DRV8847_0x04
import task_encoder_0x04
import task_motor_0x04
import task_user_0x04
import shares_0x04

def main():
    '''! @brief      The main program for Project 0x04.
    '''
    ## @brief       Task periods
    #  @details     Dict data type holding different task periods (in units
    #               of milliseconds).
    period          = {"enc_task": 10,
                       "motor_task": 10,
                       "user_task": 25}

    ## List containing relevant data from Motor 1 for sharing.
    M1_data = [1, 0, 0, 0, 0, False, False, [0, 0, 0], 0, False]
    ## List containing relevant data from Motor 2 for sharing.
    M2_data = [2, 0, 0, 0, 0, False, False, [0, 0, 0], 0, False]
    
    ## Items in the lists:
    #  Encoder/motor ID [index 0], current position
    #  [index 1], current delta/velocity [index 2], reference or
    #  previous position [index 3], reference or previous velocity
    #  [index 4], flag logical value for zeroing condition [index 5]
    #  (True: zero encoder requested or False: zero encoder not
    #  requested), logical value for disabling fault condition
    #  [index 6] (True: disable fault or False: do not disable fault),
    #  pid controller variable [index 7], duty cycle [index 8], and
    #  flag logical value for fault condition [index 9] (True: there
    #  is a fault or False: there is no fault detected)

    ## Motor driver object of the DRV8847 class
    motor_drv   = DRV8847_0x04.DRV8847(3)

    ## Data sharing object containing shared data for the first motor.
    M1_shares = shares_0x04.Share(M1_data)
    ## Data sharing object containing shared data for the second motor.  
    M2_shares = shares_0x04.Share(M2_data)

    ## User task object for user interface.
    user_task = task_user_0x04.Task_User(period["user_task"], M1_shares, M2_shares)

    ## Encoder task object for first encoder.   
    E1_task = task_encoder_0x04.Task_Encoder(period["enc_task"], M1_shares)
    ## Encoder task object for second encoder.
    E2_task = task_encoder_0x04.Task_Encoder(period["enc_task"], M2_shares)
    
    ## Motor task object for first motor.
    M1_task = task_motor_0x04.Task_Motor(period["motor_task"], M1_shares, motor_drv)
    ## Motor task object for second motor.
    M2_task = task_motor_0x04.Task_Motor(period["motor_task"], M2_shares, motor_drv)
    
    ## List of tasks to run
    task_list = [user_task, E1_task, E2_task, M1_task, M2_task]

    while (True):
        ## Attempt to run FSM unless Ctrl+C is hit
        try:
            for task in task_list:
                task.run()

        ## Break out of loop on Ctrl+C
        except KeyboardInterrupt:
            break
        
    ## Disable motor driver and terminate
    motor_drv.disable()
    print("Program terminating...\n"
          "Thank you for using this program.")

if __name__ =='__main__':
    main()