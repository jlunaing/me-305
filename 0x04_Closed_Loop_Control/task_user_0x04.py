'''!
@file       task_user_0x04.py
@brief      This module controls and implements user interface for Project 0x04.
@details    Implements a simple user interface and facilitates interaction
            with encoder and motor task objects.

            This task handles all serial communication between user and
            backend running on Nucleo. It accepts user input from a lists of 
            available commands.   

@author     Juan Luna
@date       2021-11-13  Original file
@date       2022-12-22  Modified for portfolio update
'''

import array
import pyb
import utime

## State 0 variable, Initializing state
S0_INIT = 0
## State 1 variable, Waiting for the user to input a key command
S1_WAIT_FOR_INPUT = 1
## State 2 variable for prompting proportional gain
S2_PROMPT         = 2
## State 3 variable for prompting integral gain
S3_TIME_DELAY     = 3

# Timing variables
## Record time of 30 seconds  
record_time = 30_000
## Step response duration of 10 seconds
step_time   = 10_000

## @brief       Dictionary with descriptive names for items in shared data
#  @details     Stores descriptive names for items in the list that contains
#               shared motor data. This list is defined in main and has the 
#               following items: encoder/motor ID [index 0], current position
#               [index 1], current delta/velocity [index 2], reference or
#               previous position [index 3], reference or previous velocity
#               [index 4], flag logical value for zeroing condition [index 5]
#               (True: zero encoder requested or False: zero encoder not
#               requested), logical value for disabling fault condition
#               [index 6] (True: disable fault or False: do not disable fault),
#               pid controller variable [index 7], duty cycle [index 8], and
#               flag logical value for fault condition [index 9] (True: there
#               is a fault or False: there is no fault detected)
motor_info      = {"id": 0, "position": 1, "delta": 2, "ref_position": 3, 
                   "ref_vel": 4, "zero_flag": 5, "fix_fault": 6,
                   "pid": 7, "duty_cycle": 8, "fault_flag": 9}

## @brief       VCP object to handle serial communication over USB.
#  @details     Receive characters user types in command prompt, as they 
#               are received, using this VCP. In this way, it facilitates
#               communication between user running PuTTY and Nucleo board.
comm_reader     = pyb.USB_VCP()

## Prompts for user input
pid_prints = ["Enter positional gain (%-s/rad): ",
              "Enter integral gain (%/rad): ",
              "Enter derivative gain (%-s2/rad): ",
              "Enter step velocity (%-s2/rad): "]

class Task_User:
    '''! @brief          User task class for user interface.
         @details        Facilitates interaction between user running Putty and
                         Nucleo board using a user interface. This interface
                         gives a list of available commands and prompts user to
                         enter their desired command. 
    '''
    
    def __init__(self, period, M1_shares, M2_shares):
        '''! @brief          Construct future user task objects
             @details        Instantiates attributes related to communication
                             over virtual serial port, timing as it relates to
                             user interface and data (i.e., position or delta
                             values) print statements, and similar.
             @param  period      Period of task, specified in main script.
             @param  M1_shares   Array containing motor shared data for motor 1.
             @param  M2_shares   Array containing motor shared data for motor 2.
        '''
        
        ## Counter tracking the number of times the task has run.
        self.runs = 0 
        ## The state to run on the next iteration of the task.
        self.state = S0_INIT    
        ## Period is set as the value specified in main script.
        self.period = period
        ## Time is updated once clock reaches period plus the current time
        self.next_time = period + utime.ticks_ms()   
        
        # Relevant shared data for the motors
        ## Shares variable for motor 1
        self.M1_shares = M1_shares
        ## Shares variable for motor 2
        self.M2_shares = M2_shares

        ## @brief       List with PID controller gains
        #  @details     In order: [P gain, I gain, D gain]
        self.PID        = [0, 0, 0]

        ## Corresponding time at which gains will be written
        self.t_PID      = 0
        ## Corresponding index of gains shared variable
        self.pid_idx    = 0

    def run(self): 
        '''! @brief      Run relevant functions required by user task.
             @details    Transitions through states of the FSM and displays
                         a user interface at initialization state. Reads and
                         stores user input through serial for later usage.
        '''
        current_time = utime.ticks_ms()

        ## Update timer based on current time stamp
        if (current_time >= self.next_time):
            ## Reset timer
            self.next_time += self.period
            ## Initialization state
            if (self.state == S0_INIT):
                ## Display list of commands
                print("\033c", end = "")
                print("----------------------------------------------------\n"
                      "USER COMMANDS:\n"
                      "\tz:\tZero the position of encoder 1\n"
                      "\tZ:\tZero the position of encoder 2\n"
                      "\tp:\tPrint out the position of encoder 1\n"
                      "\tP:\tPrint out the position of encoder 2\n"
                      "\td:\tPrint out the delta for encoder 1\n"
                      "\tD:\tPrint out the delta for encoder 2\n"
                      "\tm:\tEnter a duty cycle for motor 1\n"
                      "\tM:\tEnter a duty cycle for motor 2\n"
                      "\tc:\tClear fault condition triggered by the DRV8847\n"
                      "\tg:\tCollect encoder 1 data for 30 s and\n"
                      "\t\tprint it to PuTTY as a comma separated list\n"
                      "\tG:\tCollect encoder 2 data for 30 s and\n"
                      "\t\tprint it to PuTTY as a comma separated list\n"
                      "\ts:\tEnd data collection prematurely.\n"
                      "\t1:\tEnter PID for step response on motor 1.\n"
                      "\t2:\tEnter PID for step response on motor 2.\n\n"
                      "\tq:\tShow list of command again.\n\n"
                      "Enter a command...\n")
                
                ## Transitions to state 1
                self.transition_to(S1_WAIT_FOR_INPUT) # Transisions to state 1
                
                self.reset_params()

            ## State 1 logic
            elif (self.state == S1_WAIT_FOR_INPUT):
                ## Read user input
                user_cmd = self.read()
                ## Store key command returned by user       
                self.write(user_cmd[0], self.M1_shares)   
                self.write(user_cmd[0] + 32, self.M2_shares)

                ## Reset list of gain values 
                if (self.PID != [0,0,0]):
                    if (utime.ticks_diff(current_time, self.t_PID) >= 1_000):
                        print("Motor start...")
                        self.motor_stepped.write(motor_info['pid'], self.PID)
                        self.PID = [0,0,0]
                     
            elif (self.state == S2_PROMPT):
                user_cmd = self.read()
                pid_values = self.get_input(0, user_cmd[0],
                                            pid_prints[self.pid_idx])
                ## Go back to State 0 the initial state when the "q" key is hit
                if user_cmd == b'q'[0]:
                    self.transition_to(S0_INIT)

                elif (pid_values != None):
                    if(self.pid_idx > 2):
                        ## Transition to state 1
                        
                        self.motor_stepped.write(motor_info['ref_vel'], pid_values)

                        self.setup_recording(step_time,
                                         self.motor_stepped.read(motor_info['id']) - 1,
                                         [motor_info['duty_cycle'], motor_info["delta"]],
                                         "Duty cycle (%), Velocity (rad/s)")
                        
                        self.t_PID = current_time
                        self.pid_idx = 0

                        self.parsing[0] = ''

                        self.transition_to(S1_WAIT_FOR_INPUT)
                        # print("Running state 1...")

                    else:
                        self.parsing[0] = ''
                        self.PID[self.pid_idx] = pid_values
                        self.pid_idx += 1
                        self.get_input(0, b'None'[0],
                                       pid_prints[self.pid_idx])
                
    def write(self, user_cmd, motor_shares):
        '''! @brief          Display information based on command selected by user.
             @details        Logic and decision making for receiving user command.
                             It prints a variety of information regarding encoders
                             and motors based on user input.

             @param user_cmd        Key command received through serial port.
             @param motor_shares    Share variable for motor data.
        '''

        ## Motor ID (motor 1 or 2), used as index.
        id = motor_shares.read(motor_info['id']) - 1
        
        ## Logic/decision-making based on user input

        # Goes back to State 0 the initial state when the esc key is hit
        if user_cmd == b'q'[0]:
            self.transition_to(S0_INIT)
            
        # If user selects "z", request for zeroing encoder is set to True.
        elif user_cmd == b'z'[0]:
            print('Encoder ' + str(id+1) + ' has been zeroed.')
            motor_shares.write(motor_info['zero_flag'], True)
        
        # If user selects "d", print encoder delta value.
        elif user_cmd == b'd'[0]:
            print('Motor ' + str(id+1) + ' velocity [rad/s]: ' + 
                             str(motor_shares.read(motor_info['delta'])))                
        
        # If user selects "p", print encoder position value.
        elif (user_cmd == b'p'[0]):
            print('Motor ' + str(id+1) + ', position [rad]: ' +
                             str(motor_shares.read(motor_info['position']))) 
            
        # If user selects "g", collect data for 30 sec.
        elif user_cmd == b'g'[0] and not self.collect_data[id]:
            self.setup_recording(record_time, id,
                                [motor_info['position'], motor_info['delta']],
                                'Position (rad), Velocity (rad/s)')
        
        # If user selects "m", prompt user to enter duty cycle
        elif user_cmd == b'm'[0]:
            # Flag for setting duty cycle is turned on
            self.construct_duty[id] = True
            self.parsing[id] = ''

        # If user selects "c", clear triggered fault condition
        elif (user_cmd == b'c'[0]):
            motor_shares.write(motor_info['fix_fault'], True)
            print("The fault has been fixed!")
            
        elif (user_cmd == b's'[0] or motor_shares.read(motor_info['fault_flag'])):
            self.tf[id] = utime.ticks_ms()
            
        ## Data recording  
        if (self.collect_data[id] and 0 == self.next_time % (2*self.period)//self.period):
            data_1 = motor_shares.read(self.to_recording[id][0])
            data_2 = motor_shares.read(self.to_recording[id][1])
            self.record_data(id, data_1, data_2)
        
        if (self.construct_duty[id]):
            output_vel = self.get_input(id, user_cmd, "Motor " + str(id+1) 
                                        + ": Enter % motor speed: ")
            if(output_vel != None):
                motor_shares.write(motor_info['ref_vel'], output_vel)

        elif (user_cmd == 49 + 33*id and not self.construct_duty[not id]):
            self.transition_to(S2_PROMPT)
            self.motor_stepped = motor_shares
            # print("Running state 2...")
            self.get_input(0,b'None'[0],pid_prints[self.pid_idx])
            self.motor_stepped.write(motor_info['pid'], [0,0,0])
        
    def read(self):     
        '''! @brief      Receives characters entered in PuTTY while program runs.
             @details    Characters read are stored as bytes using a VCP object.
                         Clears queue and returns byte value.
        '''        
        if(comm_reader.any()):
            # Reads Most recent Command
            user_cmd = comm_reader.read(1)
            # Clears Queue
            comm_reader.read()  
            
            return user_cmd
        return b' ' 

    def setup_recording(self, delta, id, to_recording, prompt):
        '''!    @brief  Sets up variables for recording part of the program.

                @param  delta           Time difference between data points
                @param  id              Motor ID (motor 1 or 2), used as index.
                @param  to_recording    Array for holding recorded data.
                @param  prompt          Header for description of data columns.
        ''' 
        print('[Motor ' + str(id+1) + ']: \nCollecting {:} data...'.format(prompt))
        self.collect_data[id] = True
        
        self.to[id] = utime.ticks_ms()
        self.tf[id] = utime.ticks_add(self.to[id], delta)
        self.to_recording[id] = to_recording
        self.prompt[id] = prompt

    def record_data(self, id, data_1, data_2):
        '''! @brief          Records position and velocity time data.
             @details        Appends recorded data to time, position, and
                             velocity arrays. Data collection lasts 30 s
                             or less if premature stop condition (S command)
                             occurs. Data is printed at the end.

             @param id      Motor ID (motor 1 or 2), used as index.
             @param data_1  Data appended to the first motor.
             @param data_2  Data appended to the second motor.
        '''   
        ## Set current time in milliseconds
        current_time = utime.ticks_ms()   

        # Formatting and printing output of "g" command.
        # Logic for "s" command in case "g" is interrupted is implemented.
        # In other words, unless manually stopped, data collection and 
        # display occurs for 30 s (30^3 ms).
        if (utime.ticks_diff(self.tf[id], current_time) <= 0):

            self.M1_shares.write(motor_info['ref_vel'], 0)
            self.M2_shares.write(motor_info['ref_vel'], 0)
            
            self.M1_shares.write(motor_info['pid'], [0,0,0])
            self.M2_shares.write(motor_info['pid'], [0,0,0])

            print('Motor ' + str(id+1) + ': Data:\n'
                  'Time [s], {:}'.format(self.prompt[id]))

            ## Print interrupted or complete 30-s timed data      
            for n in range(len(self.timeArray[id])):
                print("{:}, {:}, {:}".format(self.timeArray[id][n],
                                             self.DataArray1[id][n],
                                             self.DataArray2[id][n]))
            ## Flag for collecting data is turned off   
            self.collect_data[id]   = False
            ## Arrays are emptied when data recording and display ends
            self.timeArray[id]      = [array.array('f',[]), array.array('f',[])]
            self.DataArray1[id]     = [array.array('f',[]), array.array('f',[])]
            self.DataArray2[id]     = [array.array('f',[]), array.array('f',[])]

        ## Append data to time, position, and velocity arrays
        else:
            self.timeArray[id].append(utime.ticks_diff(current_time, self.to[id])/1000//.1/10)
            #print(utime.ticks_diff(current_time, self.to[id])/1000//.1/10)
            self.DataArray1[id].append(data_1)
            self.DataArray2[id].append(data_2) 

    def get_input(self, num, user_cmd, prompt):
        '''!    @brief      Sets duty cycle entered by user.   
                @details    Process characters entered by user when setting duty
                            cycle so that duty cycle is a valid input to feed to 
                            the motors. This algorithm accepts one value at the
                            time and can take minus, backspace, and enter 
                            characters as ASCII bytes. 

                @param  num         Motor ID (motor 1 or 2), used as index.
                @param  user_cmd    Character entered in PuTTY by user.
                @param  prompt      Header for description of data columns.

                @return duty        Pre-processed duty cycle.
        '''
        user_cmd = user_cmd - (32*num)

        # Accept one value, between 0 and 9, at the time.
        if (user_cmd >= b'0'[0] and user_cmd <= b'9'[0]):
            new = str(user_cmd-48)
            self.parsing[num] = self.parsing[num] + new
        elif user_cmd == 127:
            self.parsing[num] = self.parsing[num][:-1]
        # If user presses the period key
        elif user_cmd == 46:
            if "." not in self.parsing[num]:
                self.parsing[num] = self.parsing[num] + '.'
        # If user presses the minus key
        elif user_cmd == 45:
            if self.parsing[num] == '' or self.parsing[num][0] != '-':
                self.parsing[num] = '-'+self.parsing[num]
        # If user presses carriage return
        elif user_cmd == 13:
            # Flag for setting duty cycle is turned off
            self.construct_duty[num] = False
            # If no characters, set duty cycle to zero
            if (self.parsing[num] == '' or self.parsing[num] == '.' 
                                        or self.parsing[num] == '-'):
                self.parsing[num] = '0'
            str_temp = self.parsing[num]
            self.parsing[num] == ''
            # Convert string to a float number
            return float(str_temp)
        # If user presses the space bar
        if (user_cmd != 32):
            print("\033c", end = "")
            print(prompt + str(self.parsing[num]))
        return None
        
    def transition_to(self, new_state):
        '''! @brief              Transitions between FSM states.
             @param  new_state   New state UI is transitioning to.
        '''
        self.state = new_state
        
    def reset_params(self):
        '''!
        @brief  Initializes and resets variables after tasks are completed.
        '''
        
        self.pid_idx = 0
        
        ## @brief           Data collection logical value for motors 1 and 2.
        #  @details         Initialized as false, the data collection flag
        #                   becomes true when user request data collection
        #                   through the G command.
        self.collect_data   = [False, False]
        
        ## @brief           Set duty cycle logical value for motors 1 and 2.
        #  @details         Initialized as false, the construct duty cycle
        #                   flag is raised when user enters duty cycle
        #                   through the M command.
        self.construct_duty = [False, False]
        ## @brief           List with characters to construct duty cycle.
        #  @details         Characters entered by the user when entering the
        #                   desired duty cycle will be pre-processed so that
        #                   a valid value for the duty cycle is entered.
        self.parsing        = ['', '']
        
        ## Array for keeping track of end time
        self.tf = array.array('l', [0, 0])
        ## Array for keeping track of begin time
        self.to = array.array('l', [0, 0])
        ## Array for holding recorded data
        self.to_recording = [[0, 0], [0, 0]]
        ## Header for description of data columns.
        self.prompt = ['', '']
        
        ## Array for time data collected for g command
        self.timeArray = [array.array('f', []), array.array('f', [])]
        ## First array for data collected for g command
        self.DataArray1 = [array.array('f', []), array.array('f', [])]
        ## Second array for data collected for g command
        self.DataArray2 = [array.array('f', []), array.array('f', [])]