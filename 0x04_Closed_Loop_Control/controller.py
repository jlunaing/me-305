'''!    @file       controller.py
        @brief      A driver for implementing closed-loop speed control.
        @details    Closed-loop speed control on the output shaft of the
                    motor using error values from reference and current 
                    values computed in encoder/motor tasks. 

        @author     Juan Luna
        @date       2021-11-13  Original file
        @date       2022-12-22  Modified for portfolio update
'''
import utime

class Controller:
    '''!    @brief      Closed-loop controller class.
            @details    Allows future controller objects to be instantiated.
                        Takes input parameters measured and reference values and
                        returns actuation signal after computing controller output.               
    '''
        
    def __init__ (self, PID, sat_limit):
        '''!    @brief      Constructs an object of the Controller class.
                @details    Setup future objects given appropriate parameters 
                            such as the initial proportional gain for the
                            system and saturation limits on the PWM level.
            
                @param  PID         List of PID parameters Kp, Ki, and Kd.
                @param  sat_limit   Duty cycle saturation limit value.
        '''
        
        ## PID controller object with gains
        self.PID = PID
        ## Saturation limit on duty cycle
        self.sat_limit = sat_limit

        ## ERROR ATTRIBUTES
        #  The sum of errors over a time difference
        self.e_sum = 0
        #  Previous error
        self.last_e = 0

    def update (self, ref_value, read_value, dt):
        '''! @brief      Computes and returns actuation value.
             @details    Actuation value based on measured and reference values.
                         Calculates the motor duty cycle that is needed based 
                         on closed-loop control (negative feedback).

             @param     ref_value   Reference or setpoint value
             @param     read_value  Actual or measured value
             @param     dt          Change in time between points

             @return        Saturated value for duty cycle
        '''       
        ## Error in the signal as the difference between reference and
        #  current (from input) values.
        err = ref_value - read_value
        ## Update sum of error by integrating over time
        self.e_sum += (self.last_e + err)*dt/2

        ## Error in delta from the difference in error values over time
        delta_e = (err - self.last_e)/dt
        ## Update the last error value
        self.last_e = err
        
        ## Use gain and error values to calculated duty cycle
        duty = (err*self.PID[0]) + (self.e_sum*self.PID[1]) + (delta_e*self.PID[2]) 
        
        return self.saturate(duty)
                
    def get_PID(self):
        '''!    @brief      Returns PID object.
        '''
        return self.PID
    
    def set_PID(self, PID):
        '''!    @brief      Allows modification of the PID gain value.
                @details    Sets new gain values for Kp, Ki, and Kd.
        '''
        self.PID = PID
        
    def saturate(self, duty):
        '''!    @brief      Logic for saturation on duty cycle
                @details    Prevents duty cycle from being set outside of the 
                            allowed range (-100 to 100).
        '''
        if (duty < self.sat_limit[0]):
            return self.sat_limit[0]

        elif (duty > self.sat_limit[1]):
            return self.sat_limit[1]

        return duty