'''!
@file       encoder_0x04.py
@brief      A driver for working with a quadrature incremental encoder.
@details    Encoder driver, in the form of a class, instantiates timers
            and channels for encoder objects pins. Methods of the Encoder
            class are defined to find encoder position and corresponding
            change in position, delta, based on an algorithm that
            accounts for overflow or underflow. 

            This driver file is imported in @c task_encoder_0x04.py, task file
            which does the actual task implementation.

@author     Juan Luna
@date       2021-11-13  Original file
@date       2022-12-22  Modified for portfolio update
'''
import pyb

class Encoder:
    '''! @brief      Encoder driver class to interface with quadrature encoders.
         @details    Allows future encoder objects to be instantiated. As such, 
                     this class contains the basic encoder functionality in the  
                     form of attributes and methods that be imported to  
                     task_encoder when task_user requests encoder data.
    '''
    
    def __init__(self, tim_ID, pin_ch1, pin_ch2):
        '''!
        @brief      Constructs an object of the Encoder class.
        @details    Initializes attributes of new objects of the class Encoder.
                    Some of these include timers, counters, and attributes 
                    related to encoder position and delta (velocity) values.
        
        @param      tim_ID      Timer ID for encoder timer object.
        @param      pin_ch1     First pin associated with encoder timer channel.
        @param      pin_ch2     Second pin associated with encoder timer channel.
        '''
        ## @brief       Timer period
        #  @details     The period specifies the 
        #               largest number that can be stored in the timer. This 
        #               value is the largest 16-bit number (2^16 - 1) or 65,535.
        self.tim_period = 2**16
        ## @brief       Encoder timer object with specified prescalar and period.
        #  @details     A prescalar of zero means that every encoder tick 
        #               corresponds to one timer count. The period specifies the 
        #               largest number that can be stored in the timer. 
        #               Setting the period at this value gives the longest window
        #               so that we have the most update calls before overflow
        #               or underflow occurs.
        self.timer_enc  = pyb.Timer(tim_ID, prescaler = 0,
                                            period = self.tim_period - 1)

        ## @brief       First timer channel configured in Encoder mode.
        #  @details     Indicates whether tick occurs.
        #               The pin to be configured for this channel will be defined 
        #               in the task corresponding to the encoder.
        self.tim_ch1    = self.timer_enc.channel(1, mode = pyb.Timer.ENC_AB,
                                                    pin  = pin_ch1)
        ## @brief       Second timer channel configured in Encoder mode.
        #  @details     Indicates whether a tick occurs when other channel is on.
        self.tim_ch2    = self.timer_enc.channel(2, mode = pyb.Timer.ENC_AB,
                                                    pin  = pin_ch2)
        
        ## @brief           Uncorrected encoder position.
        #  @details         Get encoder timer count (position) that goes up to 
        #                   max count of "2^16 - 1" due to overflow or underflow.
        self.raw_position   = self.timer_enc.counter()

        ## @brief           Corrected encoder position.
        #  @details         Get encoder timer count (position) after correction
        #                   algorithm is executed. Registered position grows 
        #                   indefinitely (theoretically) in either positive and 
        #                   negative directions.
        self.true_position  = self.timer_enc.counter()

        ## @brief           Change in encoder position (delta).
        #  @details         Encoder delta registering change in position between 
        #                   the last two update() calls.
        self.delta_tick = 0

        ## Message displayed in user interface
        print("Creating encoder object...")

    def update(self):
        ''' @brief      Updates encoder position and delta.
        '''
        self.update_delta()
        self.true_position = self.get_position() + self.get_delta()
        self.raw_position = self.timer_enc.counter()

    def update_delta(self):
        ''' @brief      Returns encoder delta value
        '''
        ## Update delta value
        delta = self.timer_enc.counter() - self.raw_position
        
        ## Accounting for overflow or underflow
        if (delta > 0.5*self.tim_period):
            delta -= self.tim_period

        elif (delta < -0.5*self.tim_period):
            delta += self.tim_period
        
        self.delta_tick = delta
        return delta
        
    def get_position(self):
        '''! @brief      Returns encoder position.
             @return     Corrected position of the encoder shaft.
        '''
        return self.true_position

    def set_position(self, position):
        '''! @brief              Sets encoder position.
             @param  position    The new position of the encoder shaft.
        '''
        self.true_position = position

    def get_delta(self):
        '''! @brief      Returns encoder delta.
             @return     Change in position of the encoder shaft between
                         the two most recent update calls.                                
        '''
        return self.delta_tick
        
        