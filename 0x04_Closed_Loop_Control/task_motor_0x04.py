'''!    @file       task_motor_0x04.py
        @brief      This module implements a motor task for Project 0x04.
        @details    Implements functionality of motor driver from 
                    @c DRV8847_0x04.py into a motor task that interacts with the
                    user-interface and encoder tasks, @c task_user_0x04.py and 
                    @c task_encoder_0x04.py respectively, in the main file 
                    @c main_0x04.py, where these tasks are imported as modules.

                    In addition, this module implements closed-loop speed 
                    control functionality using a motor controller driver
                    defined in the @c controller.py file.  

        @author     Juan Luna
        @date       2021-11-02  Original file
        @date       2022-12-22  Modified for portfolio update
'''

import pyb
import utime
import controller

## @brief       Dictionary with descriptive names for items in shared data
#  @details     Stores descriptive names for items in the list that contains
#               shared motor data. This list is defined in main and has the 
#               following items: encoder/motor ID [index 0], current position
#               [index 1], current delta/velocity [index 2], reference or
#               previous position [index 3], reference or previous velocity
#               [index 4], flag logical value for zeroing condition [index 5]
#               (True: zero encoder requested or False: zero encoder not
#               requested), logical value for disabling fault condition
#               [index 6] (True: disable fault or False: do not disable fault),
#               pid controller variable [index 7], duty cycle [index 8], and
#               flag logical value for fault condition [index 9] (True: there
#               is a fault or False: there is no fault detected)
motor_info      = {"id": 0, "position": 1, "delta": 2, "ref_position": 3, 
                   "ref_vel": 4, "zero_flag": 5, "fix_fault": 6,
                   "pid": 7, "duty_cycle": 8, "fault_flag": 9}

# MOTOR PINS

## @brief       First MCU control pin associated with first motor.
#  @details     First motor is connected to pins OUT1 and OUT2,
#               which are commanded by the MCU pins IN1 AND IN2.
pin_IN1          = pyb.Pin(pyb.Pin.cpu.B4)
## @brief       Second MCU control pin associated with first motor.
#  @details     First motor is connected to pins OUT1 and OUT2,
#               which are commanded by the MCU pins IN1 AND IN2.
pin_IN2          = pyb.Pin(pyb.Pin.cpu.B5)
## @brief       First MCU control pin associated with second motor.
#  @details     Second motor is connected to pins OUT3 and OUT4,
#               which are commanded by the MCU pins IN3 and IN4.
pin_IN3          = pyb.Pin(pyb.Pin.cpu.B0)
## @brief       Second MCU control pin associated with second motor.
#  @details     Second motor is connected to pins OUT3 and OUT4,
#               which are commanded by the MCU pins IN3 and IN4.
pin_IN4          = pyb.Pin(pyb.Pin.cpu.B1)

class Task_Motor:
    '''! @brief      Task implementing motor functionality in closed-loop control.
    '''
    
    def __init__(self, period, motor_shares, motor_driver):
        '''! @brief              Constructs future motor task objects
             @details            Instantiates objects responsible for timing tasks,
                                 data sharing, and motor objects.
             @param  period          Period of task, specified in main script.
             @param  motor_shares    Array containing motor shared data.
             @param  motor_driver    Motor driver object of the @c DRV8847 class.
        '''

        ## Period, in microseconds, defined in main program
        self.period = period
        ## Time stamp when timer reaches the next period
        self.next_time = utime.ticks_ms() + period
        ## Time stamp of last run
        self.start_time = utime.ticks_ms()

        ## Relevant shared data for the motors
        self.shared_data = motor_shares
        ## Create a motor driver object of the DRV8847 class
        self.DRV8847_drv = motor_driver

        # Two motor objects of the DRV8847 motor driver class
        #  These are separate motor objects that cab be used independently
        #  but having the parent's object configuration.
        if (self.shared_data.read(motor_info["id"]) == 1):
            ## First motor object
            self.motor = self.DRV8847_drv.motor(1, pin_IN1, pin_IN2)
        
        elif (self.shared_data.read(motor_info["id"]) == 2):
            ## Second motor object
            self.motor = self.DRV8847_drv.motor(3, pin_IN3, pin_IN4)

        ## Bring DRV8847 out of sleep mode
        self.DRV8847_drv.enable()

        ## Controller objects that reads PID values and sets duty cycle limits of
        #  100 in other direction (i.e., clockwise or counterclockwise).  
        self.controller = controller.Controller(self.shared_data.read(motor_info["pid"]),
                                                                       [-100, 100])
    
    def run(self):
        '''!
        @brief      Disables fault condition and sets duty cycle
        @details    Access user commands from shared data to clear fault 
                    condition and set the duty cycle. If fault condition is 
                    disabled, duty cycle is reset to zero. 
        '''

        ## Update timer based on current time stamp
        if (utime.ticks_ms() >= self.next_time):
            self.next_update()

            ## Update fault flag value of shared variable if fault is triggered.
            if (self.DRV8847_drv.fault_status()):
                self.shared_data.write(motor_info["fault_flag"], True)
            
            ## Reference velocity and pid values are rest and motor is enabled
            #  when disabling fault condition flag.
            if (self.shared_data.read(motor_info["fix_fault"])):

                self.shared_data.write(motor_info["ref_vel"], 0)
                self.shared_data.write(motor_info["pid"], [0, 0, 0])     

                self.DRV8847_drv.enable()
                ## Update shared variables corresponding to fault condition.
                self.shared_data.write(motor_info["fault_flag"], False)
                self.shared_data.write(motor_info["fix_fault"], False)

            ## Update PID values if new PID values are different the previous values
            if (self.shared_data.read(motor_info["pid"]) != self.controller.get_PID()):
                self.controller.set_PID(self.shared_data.read(motor_info["pid"]))

    def next_update(self):
        '''!
        @brief      Updates encoder position and updates timer period.          
        @details    Encoder object is updated based on functionality in
                    encoder driver             
        '''
        ## Time difference between consecutive runs
        dt = utime.ticks_diff(utime.ticks_ms(), self.start_time)
        self.next_time += self.period
        self.start_time = utime.ticks_ms()

        ## Update duty cycle obtained from controller update method.
        duty_cycle = self.controller.update(self.shared_data.read(motor_info["ref_vel"]),
                                            self.shared_data.read(motor_info["delta"]), dt)
        
        self.shared_data.write(motor_info["duty_cycle"], duty_cycle)
        self.motor.set_duty(duty_cycle)