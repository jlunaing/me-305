'''!
@file       shares_0x04.py
@brief      Task sharing library implementing both shares and queues.
@details    Implements data sharing between the tasks involved in Project 0x04,
            namely, the encoder, motor and user-interface tasks.

@author     Juan Luna
@date       2021-11-02  Original file
@date       2022-12-22  Modified for portfolio update
'''

class Share:
    '''! @brief      A standard shared variable.
         @details    Values can be accessed with read() or changed with write()
    '''
    def __init__(self, init_array = None):
        '''! @brief              Constructs a shared variable
             @param  init_array  Optional initial value for shared variable.                        
        '''
        self._buffer = init_array
    
    def write(self, ith, item):
        '''! @brief          Updates value of shared variable at ith index.
             @param  ith     Index ith of the new value of the shared variable.
             @param  item    New value for the shared variable at ith index.
        '''
        self._buffer[ith] = item
        
    def read(self, ith):
        '''! @brief          Access value of shared variable at ith index.
             @return         The value of the shared variable at ith index.
        '''
        return self._buffer[ith]
    
    def read_all(self):
        '''! @brief          Access the value of the shared array.
             @return         All values of shared variables contained in array.
        '''
        return tuple(self._buffer)

class Queue:
    '''! @brief      A queue of shared data.
         @details    Values can be accessed with placed into queue with put() or
                     removed from the queue with get(). Check if there are
                     items in the queue with num_in() before using get().
    '''
    def __init__(self):
        '''! @brief          Constructs an empty queue of shared values.
        '''
        self._buffer = []
    
    def put(self, item):
        '''! @brief          Adds an item to the end of the queue.
             @param  item    The new item to append to the queue.
        '''
        self._buffer.append(item)
        
    def get(self):
        '''! @brief      Remove the first item from the front of the queue.
             @return     The value of the item removed.
        '''
        return self._buffer.pop(0)
    
    def num_in(self):
        '''! @brief      Find the number of items in the queue. Call before get().
             @return     The number of items in the queue.
        '''
        return len(self._buffer)