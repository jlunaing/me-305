'''!    @file       DRV8847_0x04.py
        @brief      A driver for working with a DC motor.
        @details    The motor driver encapsulates all of the functionality
                    that will be useful for interacting with the DC motor
                    from Lab 0x03 and subsequent lab assignments.

        @author     Juan Luna
        @date       2021-11-13  Original file
        @date       2022-12-22  Modified for portfolio update
'''

import pyb
import utime

class DRV8847:
    '''! @brief      A motor driver class for the DRV8847 from TI.
         @details    Objects of this class can be used to configure the DRV8847
                     motor driver and to create one or more objects of the
                     Motor class which can be used to perform motor control.
    
                     For details, refer to the DRV8847 datasheet:
                     https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''

    def __init__ (self, tim_ID):
        '''! @brief              Initializes new objects of the DRV8847 class.
             @param  tim_ID      Timer ID associated with the board.
        '''

        ## Timer object for motor with 20-kHz frequency.
        self.timer_motor = pyb.Timer(tim_ID, freq = 20_000)
        
        ## Pin objects

        ## @brief       CPU pin associated with nSLEEP DRV8847 pin
        #  @details     The "nSLEEP" motor pin associated with sleep mode is an
        #               active-high enable pin. In other words, sleep mode is
        #               active when low. The nSLEEP pin will be low by default
        #               so that motor driver will be in sleep mode by default.
        #               A15 is the CPU pin to which nSLEEP pin is connected to.                     
        self.pin_sleep  = pyb.Pin(pyb.Pin.cpu.A15)
        ## @brief       CPU pin associated with nFAULT DRV8847 pin
        #  @details     Fault conditions are indicated by nFAULT motor pin.
        #               B2 is the CPU pin to which nFAULT pin is connected to.
        self.pin_fault  = pyb.Pin(pyb.Pin.cpu.B2)

        ## Flag for fault conditions
        self.fault_flag = False

        ## @brief       External interrupt triggered by motor fault conditions.
        #  @details     The DRV8847 driver is capable of detecting fault 
        #               conditions that may damage the motor or the motor 
        #               driver. This interrupt will disable the motor when a 
        #               fault condition occurs. The associated callback 
        #               function is fault_cb.
        self.fault_int  = pyb.ExtInt(self.pin_fault,
                                     mode = pyb.ExtInt.IRQ_RISING,
                                     pull = pyb.Pin.PULL_NONE,
                                     callback = self.fault_cb)

    def enable (self):
        '''! @brief      Brings the DRV8847 out of sleep mode.
        '''
        ## Disable motor fault external interrupt.
        self.fault_int.disable()
        ## Re-enable motor driver by bringing it out of sleep mode.
        self.pin_sleep.high()
        ## Wait for the fault pin to return high
        utime.sleep_us(25)
        ## Lower flag for fault conditions
        self.fault_flag = False
        ## Re-enable motor fault external interrupt.
        self.fault_int.enable()

        print("Motor enabled...")

    def disable (self):
        '''! @brief      Puts the DRV8847 in sleep mode.
        '''
        ## Enter sleep mode, disabling motors.
        self.pin_sleep.low()
    
    def fault_cb (self, IRQ_src):
        '''! @brief      Interrupt callback function to run on fault condition.
             @param  IRQ_src     The source of the interrupt request.
        '''
        print("A fault has occurred.\n")
        ## Raise flag for fault condition
        self.fault_flag = True
        ## Disable fault callback function
        self.disable()
    
    def fault_status(self):
        '''! @brief      Returns fault condition status as indicated by flag.
        '''
        return self.fault_flag

    def motor (self, motor_ch, pin_ch1, pin_ch2):
        '''! @brief      Initializes motor object associated with DRV8847 driver.
             @return     An object of class Motor
        '''
        return Motor(motor_ch, pin_ch1, pin_ch2, self.timer_motor)

class Motor:
    '''! @brief      A motor class for one channel of the DRV8847.
         @details    Objects of this class can be used to apply PWM to a
                     given DC motor.
    '''

    def __init__ (self, motor_ch, pin_ch1, pin_ch2, timer_motor):
        '''! @brief      Initializes motor object associated with the DRV8847.
             @details    Objects of this class will not be instantiated directly.
                         Instead DRV8847 object will be used to create future
                         Motor objects using the method DRV8847.motor().
        '''
        ## Motor timer channel 1 configured in PWM mode (active high)
        self.tim2_ch1 = timer_motor.channel(motor_ch, mode = pyb.Timer.PWM, pin = pin_ch1)
        ## Motor timer channel 2 configured in PWM mode (active high).
        self.tim2_ch2 = timer_motor.channel(motor_ch + 1, mode = pyb.Timer.PWM, pin = pin_ch2)

        self.set_duty(0)

    def set_duty (self, duty):
        '''! @brief      Set duty cycle as a pulse width percent for the motors.
             @details    Positive values represent rotation of the motor in one
                         direction (clockwise) and negative values in the
                         opposite direction (counterclockwise).

             @param  duty    Signed value representing duty cycle of
                             PWM signal to be sent to the motor pins.            
        '''
        ## Positive duty cycle
        if (duty >= 0):
            self.tim2_ch1.pulse_width_percent(duty)
            self.tim2_ch2.pulse_width_percent(0)
        ## Negative duty cycle
        elif (duty < 0):
            self.tim2_ch1.pulse_width_percent(0)
            self.tim2_ch2.pulse_width_percent(-duty)

    if __name__ == '__main__':
        # The following code is a test program for the motor class.
        # All code within the if __name__ == '__main__' block will only run 
        # when the script is executed as a standalone program. If the script 
        # is imported as a module the code block will not run.
        
        # Create a motor driver object and two motor objects. You will need 
        # to modify the code to facilitate passing in the pins and timer 
        # objects needed to run the motors.

        ## Motor driver object in source file for testing 
        motor_drv = DRV8847()
        ## First motor object in source file for testing
        motor_1 = motor_drv.motor()
        ## Second motor object in source file for testing
        motor_2 = motor_drv.motor()
        
        # Enable the motor driver
        motor_drv.enable()
        
        # Set the duty cycle of the first motor to 40 percent and 
        # that of the second motor to 60 percent
        motor_1.set_duty(40)
        motor_2.set_duty(100)