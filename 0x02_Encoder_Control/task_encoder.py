'''!
@file       task_encoder.py
@brief      This module controls and implements an encoder task for Project 0x02.
@details    Implements functionality of encoder driver from @c encoder.py
            into an encoder task that interacts with a user interface task.
            This interaction occurs in a main file, where this script and 
            @c task_user.py are imported as modules.

@date       2021-10-19  Original file
@date       2022-12-22  Modified for portfolio update 
'''

import encoder
import utime
import pyb

## @brief Sets up first pin for encoder 1
pinA = pyb.Pin(pyb.Pin.cpu.B6)
## @brief Sets up second pin for encoder 1
pinB = pyb.Pin(pyb.Pin.cpu.B7)

class Task_Encoder:
    '''! @brief     Encoder task implementing previously defined encoder driver.
         @details   Tracks encoder position and delta. Communicates with 
                    @c task_user.py module to set encoder position to zero when
                    user interface determines it is appropriate. 
    '''
    
    def __init__(self, period):
        '''! @brief              Constructs future encoder task objects
             @details            Instantiates attributes related to timing,
                                 encoder objects, and data storage (i.e., tuple
                                 containing position and delta values).
             @param     period  User task period, specified in main script
        '''
        
        ## Period, in milliseconds, defined in main program
        self.period = period
        ## Time adjusts once clock reaches the period value plus the current time
        self.next_time = period + utime.ticks_ms()
        
        ## @brief Creates encoder object
        #  @details First encoder object of the Encoder class.
        #           Input parameters specify encoder pins and timer.
        self.encoder_1 = encoder.Encoder(pinA, pinB, 4)
        
        ## Tuple containing new encoder position and delta values.  
        self.lastUpdate = (0,0)
    
    def run(self, setZero):
        '''!
        @brief          Resets encoder position and updates position and delta.
        @details        This method is run in main script to set encoder position
                        to zero ("zeroing encoder") when relevant flag is triggered.
                        This occurs in UI task when user presses "z" command.   
        @param setZero  Logical value that is is true when "z" command is entered.
        @return         Next update to obtaine future encoder position and delta.
        '''
        # If "z" command is entered (setZero is True), then encoder position is reset.
        if (setZero == 1):
            self.encoder_1.set_position(0)
        # Next update to obtain new encoder position and delta
        if (utime.ticks_ms() >= self.next_time):
            self.lastUpdate = self.nextUpdate()
            
        return self.lastUpdate
        
    def nextUpdate(self):
        '''!
        @brief      Updates encoder position and updates period timer clock.          
        @details    Encoder object is updated based on functionality in encoder driver
        @return     Tuple containing new encoder position and delta values.                
        '''
        self.encoder_1.update() 
        self.next_time += self.period
        return (self.encoder_1.get_position(),self.encoder_1.get_delta())