'''!
@file       main_0x02.py
@brief      Main script for overall execution of Project 0x02: Encoder Control.
@details    Imports @c task_encoder.py and @c task_user.py modules to read
            and set position and change in position (delta) of encoder and
            print this information in a user interface. The user has the
            ability to select an action from a list of commands to display
            certain information related to the state of the encoder.

            More details in 
            <a href = "https://bitbucket.org/jlunaing/me-305/src/master/0x02_Encoder_Control/">
            source code</a> and @ref writeup_0x02_encoder "documentation".

            State-transition diagram for FSM:

            @image  html    0x02_fsm.png "" width = 85%
            
@author     Juan Luna
@date       2021-10-19  Original file
@date       2022-12-22  Modified for portfolio update  
'''

import task_encoder
import task_user

## Encoder task period, set to 2 ms
EncoderTaskPer = 2
## User task period, set to 50 ms
UserIntTaskPer = 50

if __name__ =='__main__':

    ##  @brief      Encoder task object of the Task_Encoder class.
    #   @details    This tasks runs at period set by EncoderTaskPer.   
    lab2_encoderTask = task_encoder.Task_Encoder(EncoderTaskPer)
    
    ##  @brief      User task object of the Task_User class.
    #   @details    This tasks runs at period set by UserIntTaskPer.
    lab2_userTask = task_user.Task_User(UserIntTaskPer)
    
    ##  @brief      Logical variable controlling "zeroing" of the encoder
    #   @details    When user selects "z" command, a "request" to set encoder
    #               to zero position is set to True (1). If no such command
    #               is received, then this flag remains unraised (False (0)).
    #               While many user commands are available, part of the logic  
    #               involving the "z" command lives outside of the "task_user"
    #               module since, to a degree, it resets many variables. Thus,
    #               it is significant.               
    setZero = 0
         
    while (True):
        # Attempt to run FSM unless Ctrl+C is hit
        try:
            # Encoder task is executed and a tuple containing encoder position
            # and change in position (delta) is returned.

            ## Tuple containing position and delta values of encoder
            encTuple = lab2_encoderTask.run(setZero)
            
            # Tuple containing position and delta of encoder is sent to user
            # interface task for corresponding display on screen.
            setZero = lab2_userTask.run(encTuple)

        except KeyboardInterrupt:   # Break out of loop on Ctrl+C
            break
        
    print("Program terminating...")   # Clean up on exit