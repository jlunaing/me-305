'''!
@file       encoder.py
@brief      A driver for working with an encoder.
@details    Encoder driver, in the form of a class, instantiates timers
            and channels for encoder objects pins. Methods of the Encoder
            class are defined to find encoder position and corresponding
            change in position, delta, based on an algorithm that
            accounts for overflow or underflow. 

            This driver file will be imported to the @c task_encoder.py
            file which does the actual task implementation.

@author     Juan Luna
@date       2021-10-19  Original file
@date       2022-12-22  Modified for portfolio update
'''
import pyb

class Encoder:
    '''! @brief      Encoder driver class to interface with quadrature encoders.
         @details    Allows future encoder objects to be instantiated. As such, this
                     class contains the basic encoder functionality in the form of 
                     attributes and methods that be imported to @c task_encoder.py 
                     when @c task_user.py requests encoder data.
    '''
    
    def __init__(self, pin1, pin2, timerID):
        '''! 
        @brief      Constructs an object of the Encoder class.
        @details    Initializes attributes of new objects of the class Encoder.
                    Some of these include timers, counters, and attributes related
                    to encoder position and change in position (delta).
        
        @param      pin1 defines channel one pin in task encoder
        @param      pin2 defines channel two pin in task encoder
        @param      timerID defines the timer number that will be used for pyb Timer.
        '''
        ## @brief       Encoder period set as the largest 16-bit number. 
        #  @details     (2^16 - 1) or 65,535 gives the longest window so that we have
        #               the most update calls before overflows or underflows occur.
        #               Period will be set up when defining timer object.
        self.encoderPeriod = 2**16 - 1;

        ## @brief Timer object that counts up to 2^16-1, before overflow/underflow occurs.
        self.encTimer = pyb.Timer(timerID, prescaler = 0, period = self.encoderPeriod)

        ## @brief   First channel for timer.
        #  @details Indicates whether tick occurs.
        self.encTimer.channel(1, mode = pyb.Timer.ENC_B, pin = pin1)
        ## @brief   Second channel for timer.
        #  @details Indicate whether a tick occurs when other channel is on.
        self.encTimer.channel(2, mode = pyb.Timer.ENC_A, pin = pin2)
        
        ## @brief Uncorrected encoder position.
        #  @details Encoder position up to max count of 2^16 - 1 due to effects of
        #           overflow or underflow.
        self.rawPosition = self.encTimer.counter();
        ## @brief Corrected encoder position.
        #  @details Encoder position after correction algorithm is implemented.
        #           Registered position is not limited by overflow or underflow.
        self.truePosition  = self.encTimer.counter();
        
        print("Creating encoder object...\n")

    def update(self):
        '''! @brief      Updates encoder position and delta.
        '''
        self.truePosition = self.get_position() + self.get_delta()
        self.rawPosition = self.encTimer.counter()
        
    def get_position(self):
        '''! @brief      Returns encoder position.
             @return     The position of the encoder shaft.
        '''
        return self.truePosition

    def set_position(self, position):
        '''! @brief              Sets encoder position.
             @param  position    The new position of the encoder shaft.
        '''
        self.truePosition = position

    def get_delta(self):
        '''! @brief      Returns encoder delta
             @return     Change in position of the encoder shaft between
                         the two most recent updates                                
        '''  
        deltaTick = self.encTimer.counter() - self.rawPosition
        
        # Accounting for overflow or underflow
        if (deltaTick > 0.5*self.encoderPeriod):
            return deltaTick - self.encoderPeriod
        elif (deltaTick < -0.5*self.encoderPeriod):
            return deltaTick + self.encoderPeriod
        return deltaTick