'''!
@file       task_user.py
@brief      This module controls and implements user interface for Project 0x02.
@details    Implements a simple data collection interface and facilitates
            interaction with encoder task objects. This interaction occurs in  
            a main file @c main.py. Specifically, this file, as well as
            @c task_encoder.py, are imported as modules.

            This task handles all serial communication between user and
            backend running on Nucleo. It accepts user input from a lists of 
            available commands.     

@author     Juan Luna
@date       2021-10-19  Original file
@date       2022-12-22  Modified for portfolio update
'''

import utime
import pyb

## State 0 variable representing "initializion" state.
S0_INIT = 0
## State 1 variable representing "waiting for input" state.
S1_WAIT_FOR_INPUT = 1

# Define a class for user interface
class Task_User:
    '''! @brief          User task class for user interface.
         @details        Facilitates interaction between user running Putty and
                         Nucleo board using a user interface. This interface
                         gives a list of available commands and prompts user to
                         enter their desired command. 
    '''
    
    def __init__(self, period):
        '''! @brief          Construct future user task objects
             @details        Instantiates attributes related to communication
                             over virtual serial port, timing as it relates to
                             user interface and data (i.e., position or delta
                             values) print statements, and similar.
             @param period   User task period, specified in main script.
        '''
     
        ## The state to run on the next iteration of the task.
        self.state = S0_INIT
        ## Counter tracking the number of times the task has run.
        self.runs = 0
        
        ## @brief       VCP object to handle serial communication over USB.
        #  @details     Receive characters user types in command prompt,
        #               as they are received, using this VCP. In this way, it
        #               facilitates communication between user running PuTTY
        #               and Nucleo board.
        self.serialComm = pyb.USB_VCP()
        
        ## Period is set as per the value specified in main script.
        self.period = period
        ## Time is updated once clock reaches period plus the current time
        self.nextTime = period + utime.ticks_ms()
        ## Current time
        self.startTime = utime.ticks_ms()
        
        ## @brief Array for time data collected for g" command
        self.timeArray = []
        ## @brief Array for position data collected for g command
        self.PosArray = []
        
        ## Logical value that turns 1 once "g" command is received.
        self.displayPosition = 0

    def run(self, encTuple): 
        '''! @brief     Runs helper functions for user task
             @details   Transitions through states for every period and prints 
                        out user interface.
             @param     encTuple   Tuple containing encoder position and delta values.
        '''
        # Checks if current time is past the current time plus the period before running
        if (utime.ticks_ms() >= self.nextTime): 
            # Resets new time for function to run
            self.nextTime += self.period
            
            # State 0 code
            if (self.state == S0_INIT):
                print("---------------------------------------------\n"
                      "USER COMMANDS:\n"
                      "\tz:\tZero the position of encoder\n"
                      "\tp:\tPrint out the position of encoder\n"
                      "\td:\tPrint out the delta for encoder\n"
                      "\tg:\tCollect encoder 1 data for 30 s and\n"
                      "\t\tprint it to PuTTY as a comma separated list\n"
                      "\ts:\tEnd data collection prematurely.\n"
                      "\tq:\tShow list of command again.\n\n"
                      "Enter a command...\n")
                
                # Transision to state 1
                self.transition_to(S1_WAIT_FOR_INPUT) 
            
            # State 1 code
            elif (self.state == S1_WAIT_FOR_INPUT):
                # Store returned key command from read function
                userCommand = self.read()
                # Stores Boolean value if zero condition is met.       
                setZero = self.write(userCommand, encTuple)        
                return setZero  
        
        self.runs += 1
        return 0
    
    def write(self, userCommand, encTuple):
        '''! @brief          Prints results based on command selected by user.
             @details        Logic for receiving user command. It prints encoder
                             position and delta, resets encoder position to zero
                             based on Boolean logic (True = 1, False = 0).

             @param  userCommand    Key command received by read(), used in logic.
             @param  encTuple       Tuple containing encoder position and delta values.
        '''
        
        # Update tuple containing position and delta shared by task encoder
        (encPos, encDelta) = encTuple
        # Current time in ms
        timeNow = utime.ticks_ms()   
        
        # Formatting and printing output of "g" command.
        # Implement logic for "s" command in case "g" is interrupted.     
        if  (self.displayPosition == 1 and (userCommand == b's' or 
             utime.ticks_diff(utime.ticks_add(self.to, 30000), timeNow) <= 0)):
            
            print("Time (s), Position (ticks)\n"
                  "---------------------------")
            
            # Print interrupted or complete 30-s timed data
            for n in range(len(self.timeArray)):
                print("{:}\t,\t{:}".format(self.timeArray[n]/1000, self.PosArray[n]))

            # Array resets when data recording and display ends
            self.displayPosition = 0
            self.timeArray = []
            self.PosArray = []

        # Append data to time and position arrays
        elif (self.displayPosition == 1):
            self.timeArray.append(utime.ticks_diff(timeNow, self.to))
            self.PosArray.append(encPos)
        
        # If user selects "g", collect data for 30 sec.
        elif (userCommand == b'g'):
            print("Collecting Data...\n")
            self.displayPosition = 1
            ## Initial time for collecting data
            self.to = timeNow

        # If user selects "z", request for zeroing encoder is set to True.
        elif (userCommand == b'z'):
            return 1
        # If user selects "p", print encoder position from encTuple.
        elif (userCommand == b'p'):
            print("Encoder Position: " + str(encPos))  
        # If user selects "d", print delta from encTuple.
        elif (userCommand == b'd'):
            print("Encoder Delta: " + str(encDelta))                              
        # If user doesn't press "z", request for zeroing encoder is False. 
        return 0            
                      
    def read(self):     
        '''! @brief     Receives characters from PuTTY while program is running.
             @details    Characters read from user input in PuTTY are stored as 
                         bytes using a VCP object. Read function clears queue
                         and returns byte value. An additional user command, "q"
                         is included so that user can go back to state 0.
        '''       
        if(self.serialComm.any()):
            # Read Most recent command
            userCommand = self.serialComm.read(1)
            # Clear queue
            self.serialComm.read()
            
            # Go back to State 0 the initial state when the "q" key is hit
            if(userCommand == b'q'):
                self.transition_to(S0_INIT)
                return ''
            return userCommand
                
    def transition_to(self, new_state):
        '''! @brief Function responsible for transitioning between FSM states.
             @param      new_state   New state UI is transitioning to.
        '''
        self.state = new_state