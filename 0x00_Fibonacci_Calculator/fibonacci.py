'''!    @file       fibonacci.py
        @brief      This script provides the Fibonacci number at a specified index.
        @author     Juan Luna
        @date       2021-09-21  Original file
        @date       2022-12-22  Modified for portfolio update
'''

def fib (idx):
    '''!
    @brief      This function calculates a Fibonacci number at a specific index.
    @param idx An integer holding the index of the desired Fibonacci number.
    @return     Returns Fibonacci number corresponding to specified index.
    '''
    ## List holding the sequence of Fibonnaci numbers,
    #  initialized with first two Fibonnaci numbers, f0=1 and f1=1.
    fib_seq = [0,1]
    for i in range(2, idx + 1):
        fib_seq.append(fib_seq[-1] + fib_seq[-2])
    return fib_seq[idx]

if __name__ == '__main__':
    # Any code within this block will only run when the script is executed as
    # a standalone program. If the script is imported as a module, the 
    # following code block will not run.

    ## The index of the Fibonacci number selected by the user.
    idx = 0
    
    ## Boolean flag for repeating the program upon user request.
    restart_flag = True
    
    while (restart_flag == True):
        # The following ensures that the Fibonacci operation executes only
        # when the user enters an appropriate input, i.e., a non-negative
        # integer. This is accomplished using a try-except statement.
        try:
            print("Choose an index: ")
            idx = int(input())
            
            if (idx >= 0):
                print('Fibonacci number at '
                      'index {:} is {:}.'.format(idx,fib(idx)))
                ## User input that decides whether or not to continue execution
                move_on = input("Enter 'q' to quit or any other key to continue: ")
                
                if (move_on == 'q'):
                    restart_flag = False
                    print('\nThank you for using my program!')

            else:
                print('The index must be non-negative! Try again.')
            
        except ValueError:
            print('The index must be an integer! Try again.')